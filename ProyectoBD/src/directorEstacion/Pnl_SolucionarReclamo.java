/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package directorEstacion;

import controlador.controladordirectorEstacion;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author saracmh
 */
public class Pnl_SolucionarReclamo extends javax.swing.JPanel {

    ManejadoraSR manejadora;
    public Pnl_SolucionarReclamo() {
        initComponents();
        manejadora = new ManejadoraSR();
        btn_buscar.addActionListener(manejadora);
        btn_aceptar.addActionListener(manejadora);
        btn_nuevaBusqueda.addActionListener(manejadora);
        txt_ntiquete.addKeyListener(manejadora);
        txa_solucion.setEnabled(false);
        btn_aceptar.setEnabled(false);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbl_titulo = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txa_solucion = new javax.swing.JTextArea();
        btn_aceptar = new javax.swing.JButton();
        pnl_pasajero2 = new javax.swing.JPanel();
        btn_buscar = new javax.swing.JButton();
        btn_nuevaBusqueda = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txt_ntiquete = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txt_estado = new javax.swing.JTextField();
        txt_tipo = new javax.swing.JTextField();
        txt_fecha = new javax.swing.JTextField();
        txt_motivo = new javax.swing.JTextField();

        setBackground(new java.awt.Color(0, 36, 84));
        setMinimumSize(new java.awt.Dimension(850, 670));
        setLayout(null);

        lbl_titulo.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lbl_titulo.setForeground(java.awt.Color.white);
        lbl_titulo.setText("Solucionar Reclamo");
        add(lbl_titulo);
        lbl_titulo.setBounds(320, 90, 240, 32);

        txa_solucion.setColumns(20);
        txa_solucion.setRows(5);
        jScrollPane1.setViewportView(txa_solucion);

        add(jScrollPane1);
        jScrollPane1.setBounds(130, 387, 610, 140);

        btn_aceptar.setText("Aceptar");
        btn_aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_aceptarActionPerformed(evt);
            }
        });
        add(btn_aceptar);
        btn_aceptar.setBounds(690, 600, 120, 35);

        pnl_pasajero2.setBackground(new java.awt.Color(0, 36, 84));
        pnl_pasajero2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(""), "Queja", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Andale Mono", 0, 12), java.awt.Color.white)); // NOI18N
        pnl_pasajero2.setForeground(java.awt.Color.white);
        pnl_pasajero2.setLayout(null);

        btn_buscar.setText("Buscar");
        btn_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarActionPerformed(evt);
            }
        });
        pnl_pasajero2.add(btn_buscar);
        btn_buscar.setBounds(320, 30, 140, 35);

        btn_nuevaBusqueda.setText("Nueva Busqueda");
        btn_nuevaBusqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_nuevaBusquedaActionPerformed(evt);
            }
        });
        pnl_pasajero2.add(btn_nuevaBusqueda);
        btn_nuevaBusqueda.setBounds(470, 30, 140, 35);

        jLabel6.setFont(new java.awt.Font("Ubuntu", 1, 17)); // NOI18N
        jLabel6.setForeground(java.awt.Color.white);
        jLabel6.setText("No. tiquete:");
        pnl_pasajero2.add(jLabel6);
        jLabel6.setBounds(20, 30, 130, 23);
        pnl_pasajero2.add(txt_ntiquete);
        txt_ntiquete.setBounds(120, 30, 180, 30);

        add(pnl_pasajero2);
        pnl_pasajero2.setBounds(110, 130, 620, 80);

        jLabel4.setFont(new java.awt.Font("Ubuntu", 1, 17)); // NOI18N
        jLabel4.setForeground(java.awt.Color.white);
        jLabel4.setText("Solución:");
        add(jLabel4);
        jLabel4.setBounds(130, 350, 130, 23);

        jLabel5.setFont(new java.awt.Font("Ubuntu", 1, 17)); // NOI18N
        jLabel5.setForeground(java.awt.Color.white);
        jLabel5.setText("Estado:");
        add(jLabel5);
        jLabel5.setBounds(450, 290, 130, 23);

        jLabel7.setFont(new java.awt.Font("Ubuntu", 1, 17)); // NOI18N
        jLabel7.setForeground(java.awt.Color.white);
        jLabel7.setText("Motivo solicitud:");
        add(jLabel7);
        jLabel7.setBounds(450, 250, 130, 23);

        jLabel8.setFont(new java.awt.Font("Ubuntu", 1, 17)); // NOI18N
        jLabel8.setForeground(java.awt.Color.white);
        jLabel8.setText("Fecha solicitud:");
        add(jLabel8);
        jLabel8.setBounds(130, 290, 130, 23);

        jLabel9.setFont(new java.awt.Font("Ubuntu", 1, 17)); // NOI18N
        jLabel9.setForeground(java.awt.Color.white);
        jLabel9.setText("Tipo queja:");
        add(jLabel9);
        jLabel9.setBounds(130, 250, 130, 23);

        txt_estado.setEditable(false);
        add(txt_estado);
        txt_estado.setBounds(580, 290, 140, 30);

        txt_tipo.setEditable(false);
        add(txt_tipo);
        txt_tipo.setBounds(260, 240, 140, 30);

        txt_fecha.setEditable(false);
        add(txt_fecha);
        txt_fecha.setBounds(260, 290, 140, 30);

        txt_motivo.setEditable(false);
        add(txt_motivo);
        txt_motivo.setBounds(580, 240, 140, 30);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_aceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_aceptarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_aceptarActionPerformed

    private void btn_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_buscarActionPerformed

    private void btn_nuevaBusquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_nuevaBusquedaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_nuevaBusquedaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_aceptar;
    private javax.swing.JButton btn_buscar;
    private javax.swing.JButton btn_nuevaBusqueda;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbl_titulo;
    private javax.swing.JPanel pnl_pasajero2;
    private javax.swing.JTextArea txa_solucion;
    private javax.swing.JTextField txt_estado;
    private javax.swing.JTextField txt_fecha;
    private javax.swing.JTextField txt_motivo;
    private javax.swing.JTextField txt_ntiquete;
    private javax.swing.JTextField txt_tipo;
    // End of variables declaration//GEN-END:variables
public class ManejadoraSR implements ActionListener,KeyListener
{
        controladordirectorEstacion controlador = new controladordirectorEstacion();
        @Override
        public void actionPerformed(ActionEvent e) {
            
            if(e.getSource()==btn_aceptar)
            {
                String ntiquete = txt_ntiquete.getText();
                String solucion = txa_solucion.getText();
                
                if(ntiquete.equals("")||solucion.equals(""))
                {
                    JOptionPane.showMessageDialog(null,"Porfavor ingrese todos los datos antes de continuar");
                }else
                    {   
                        controlador.solucionarQuejaTiquete(ntiquete,solucion);
                    }
                
                btn_aceptar.setEnabled(false);
                txa_solucion.setEnabled(false);
                txa_solucion.setText("");
                txt_ntiquete.setText("");
                txt_estado.setText("");
                txt_fecha.setText("");
                txt_motivo.setText("");
                txt_tipo.setText("");
                btn_buscar.setEnabled(true);
                    
            }else if(e.getSource()==btn_buscar)
                {
                    String ntiquete = txt_ntiquete.getText();
                    
                    if(ntiquete.equals(""))
                    {
                        JOptionPane.showMessageDialog(null, "favor ingresar un numero de tiquet");
                    }else if(controlador.buscarTiquete(ntiquete,txt_tipo,txt_motivo,txt_fecha,txt_estado))
                        {
                            
                            if(!txt_estado.equals("terminado"))
                            {
                                btn_aceptar.setEnabled(true);
                                txa_solucion.setEnabled(true);
                                btn_buscar.setEnabled(false);
                            }
                            
                        }else
                            {
                                JOptionPane.showMessageDialog(null, "no se ha encontrado el tiquet buscado");
                            }
                }else if (e.getSource()==btn_nuevaBusqueda)
                {
                    btn_aceptar.setEnabled(false);
                    txa_solucion.setEnabled(false);
                    txa_solucion.setText("");
                    txt_ntiquete.setText("");
                    txt_estado.setText("");
                    txt_fecha.setText("");
                    txt_motivo.setText("");
                    txt_tipo.setText("");
                    btn_buscar.setEnabled(true);
                }
                    

                    
        }

        public void keyTyped(KeyEvent e) {
            
            if(e.getSource() == txt_ntiquete)
            {
                int caracter = e.getKeyChar();
                //Solo permitira que se le ingresen valores numericos al campo de texto
                if(!(caracter >= 48 && caracter <= 57))
                {
                    //Se limpia el caracter en caso de no ser un numero
                    e.setKeyChar((char) e.VK_CLEAR);
                }
            }
            if (txt_ntiquete.getText().length()== 20)
            {
                e.consume();
            }
     

            
        }

        @Override
        public void keyPressed(KeyEvent ke) {
            
        }

        @Override
        public void keyReleased(KeyEvent ke) {
            
        }
    
}


}
