/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auxiliar;

import controlador.ControladorAuxiliar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JOptionPane;

/**
 *
 * @author saracmh
 */
public class pnl_VenderTarjeta extends javax.swing.JPanel {

    /**
     * Creates new form pnl_VenderTarjeta
     */
    ManejadoraV maneja;
    ControladorAuxiliar controlador;
    String id_empleado;
    public pnl_VenderTarjeta(String cedula) {
        initComponents();
        maneja = new ManejadoraV();
        Jtxt_saldo.addKeyListener(maneja);
        btn_vender.addActionListener(maneja);
        controlador = new ControladorAuxiliar();
        id_empleado= cedula;
        controlador.cargarEstaciones(combo_estacion);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbl_titulo = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        pnl_pasajero1 = new javax.swing.JPanel();
        Jlbl_saldo = new javax.swing.JLabel();
        Jtxt_saldo = new javax.swing.JTextField();
        Jlbl_saldo1 = new javax.swing.JLabel();
        combo_estacion = new javax.swing.JComboBox();
        btn_vender = new javax.swing.JButton();

        setBackground(new java.awt.Color(0, 36, 84));
        setMinimumSize(new java.awt.Dimension(850, 670));
        setName("PanelVender"); // NOI18N
        setPreferredSize(new java.awt.Dimension(850, 670));
        setLayout(null);

        lbl_titulo.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lbl_titulo.setForeground(java.awt.Color.white);
        lbl_titulo.setText("Vender Tarjeta");
        add(lbl_titulo);
        lbl_titulo.setBounds(330, 90, 190, 28);

        jLabel2.setFont(new java.awt.Font("Ubuntu", 1, 17)); // NOI18N
        jLabel2.setForeground(java.awt.Color.white);
        add(jLabel2);
        jLabel2.setBounds(250, 240, 130, 0);

        pnl_pasajero1.setBackground(new java.awt.Color(0, 36, 84));
        pnl_pasajero1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(""), "Tarjeta", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Andale Mono", 0, 12), java.awt.Color.white)); // NOI18N
        pnl_pasajero1.setForeground(java.awt.Color.white);
        pnl_pasajero1.setLayout(null);

        Jlbl_saldo.setFont(new java.awt.Font("Ubuntu", 1, 17)); // NOI18N
        Jlbl_saldo.setForeground(java.awt.Color.white);
        Jlbl_saldo.setText("Estación:");
        pnl_pasajero1.add(Jlbl_saldo);
        Jlbl_saldo.setBounds(40, 110, 130, 21);
        pnl_pasajero1.add(Jtxt_saldo);
        Jtxt_saldo.setBounds(130, 40, 180, 27);

        Jlbl_saldo1.setFont(new java.awt.Font("Ubuntu", 1, 17)); // NOI18N
        Jlbl_saldo1.setForeground(java.awt.Color.white);
        Jlbl_saldo1.setText("Saldo:");
        pnl_pasajero1.add(Jlbl_saldo1);
        Jlbl_saldo1.setBounds(40, 40, 130, 21);

        combo_estacion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccione estación..." }));
        combo_estacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                combo_estacionActionPerformed(evt);
            }
        });
        pnl_pasajero1.add(combo_estacion);
        combo_estacion.setBounds(130, 110, 190, 27);

        btn_vender.setText("Vender");
        pnl_pasajero1.add(btn_vender);
        btn_vender.setBounds(110, 170, 120, 35);

        add(pnl_pasajero1);
        pnl_pasajero1.setBounds(250, 190, 350, 230);
    }// </editor-fold>//GEN-END:initComponents

    private void combo_estacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_combo_estacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_combo_estacionActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Jlbl_saldo;
    private javax.swing.JLabel Jlbl_saldo1;
    private javax.swing.JTextField Jtxt_saldo;
    private javax.swing.JButton btn_vender;
    private javax.swing.JComboBox combo_estacion;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lbl_titulo;
    private javax.swing.JPanel pnl_pasajero1;
    // End of variables declaration//GEN-END:variables

public class ManejadoraV implements ActionListener, KeyListener{
    
     

        @Override
        public void actionPerformed(ActionEvent e) {
            
            if(e.getSource() == btn_vender){
            String saldo = Jtxt_saldo.getText();
            String estacion = combo_estacion.getSelectedItem().toString();
            String id_estacion = controlador.consultarEstacion(estacion);
            
            if(combo_estacion.getSelectedIndex() != 0 && !Jtxt_saldo.getText().isEmpty()){
            controlador.venderTarjeta(Jtxt_saldo, saldo, id_empleado, id_estacion, combo_estacion);            
                       
            }else{
                    JOptionPane.showMessageDialog(null, "Debe ingresar el saldo y la estación.",
                         null, JOptionPane.WARNING_MESSAGE);            
            }
            }
            
        }

        @Override
        public void keyTyped(KeyEvent e) {
            if(e.getSource() == Jtxt_saldo)
            {
                int caracter = e.getKeyChar();
                //Solo permitira que se le ingresen valores numericos al campo de texto
                if(!(caracter >= 48 && caracter <= 57))
                {
                    //Se limpia el caracter en caso de no ser un numero
                    e.setKeyChar((char) e.VK_CLEAR);
                }
            }            
         }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

}

}