/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import accesoDatos.DaoConductor;

/**
 *
 * @author sara
 */
public class ControladorConductor {
    
    DaoConductor dao;
    public ControladorConductor(){
        dao = new DaoConductor();
    }
    
    
     public ArrayList consultarTurno(String id, String fecha) {
        ResultSet resultado = dao.consultarTurno(id, fecha);
        ArrayList respuesta = new ArrayList();

        try {
            while (resultado.next()) {

                respuesta.add(resultado.getString(1));
                respuesta.add(resultado.getString(2));
                respuesta.add(resultado.getString(3));

            }
            resultado.close();
        } catch (SQLException sql) {
            System.out.println(sql.getMessage());
        }

        return respuesta;

    }
}
