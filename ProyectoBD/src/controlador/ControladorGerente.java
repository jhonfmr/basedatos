/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import accesoDatos.DaoGerente;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author invitado
 */
public class ControladorGerente {

    DaoGerente dao;

    public ControladorGerente() {
        dao = new DaoGerente();
    }

    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @return ResultSet Método que consulta la informacion de los pasajeros
     */
    public ResultSet consultarPasajeros() {
        ResultSet result = dao.consultarPasajeros();
        return result;
    }

    /**
     * Responsable: Sara Catelina Muñoz H.
     * @return resultSet
     * Método que consultas los años en los que se han realizado ventas
     */
    public ArrayList consultarAhnosVentas() {
        ResultSet result = dao.consultarAnhosVentas();

        ArrayList anhos = new ArrayList();

        try {
            while (result.next()) {
                anhos.add(result.getString(1));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }

        return anhos;
    }
    
    /**
     * Resposnable: Sara Catalina Muñoz H.
     * @param anho
     * @param mes
     * @return ResultSet
     * Método que consulta la suma de las ventas realizadas en una estacion
     */
    public ResultSet consultarVentasEstaciones(String anho, String mes) {
        ResultSet result = dao.consultarVentasTarjetas(mes, anho);
        return result;
    }
 
    /**
     * Responsable: Esteban Aguirre Martinez.
     * @return String
     * Método que envia los datos para agregar un usuario (empleado) al sistema y retorna si la operacion
     * fue exitosa o no
     */
    public String agregarUsuario(String cedula, String nombre, String apellido, String cargo, String salario, String estado, String empleadoControla, String usuario, String contraseña) {
        String resultado = "fallo";
        
        resultado = dao.peticionAgregarUsuario(cedula,nombre,apellido,cargo,salario,estado,empleadoControla,usuario,contraseña);
        
        return resultado;
    }

    /**
     * Responsable: Esteban Aguirre Martinez.
     * @return void
     * Método consulta una lista de los usuarios segun el cargo elegido o en su defecto
     * consulta la informacion de todos los usuarios y llena una tabla con la informacion
     */
    public void listarUsuariosCargo(JTable tablaUsuarios,String cargo)
    {
            ResultSet consulta = dao.consultarUsuariosCargo(cargo);
            DefaultTableModel modelo = new DefaultTableModel();
            boolean todos=false;
            
            //se verifica si se pide la informacion de todos los usuarios o de uno solo
            if(cargo.equals("Todos"))
            {
                todos=true;
            }
            
            tablaUsuarios.setModel(modelo);
            try {
            //se llena las cabeceras de las columnas de la tabla segun si se elige un cargo
            //en particular o si se pide la informacion de todos los empleados
            if(!todos)
            {
                modelo.addColumn("id empleado");
                modelo.addColumn("nombre");
                modelo.addColumn("apellido");
                modelo.addColumn("estado");
            }else
                {
                modelo.addColumn("id empleado");
                modelo.addColumn("nombre");
                modelo.addColumn("apellido");
                modelo.addColumn("cargo");
                modelo.addColumn("estado");
                }
                
            //Recorro el ResultSet que contiene los resultados.
            int contador = 0;

            //se llena la tabla con los registros de la consulta
            while (consulta.next()) {
                if(todos)
                {
                   Object[] ob = new Object[5];//Crea un vector para almacenar los valores del ResultSet
                    ob[0] = (consulta.getString(1));
                    ob[1] = (consulta.getString(2));
                    ob[2] = (consulta.getString(3));
                    ob[3] = (consulta.getString(4));
                    ob[4] = (consulta.getString(5));
                    contador++;
                    //Se añade la fila al modelo
                    modelo.addRow(ob);
                    ob = null;//limpia los datos de el vector de la memoria 
                }else
                    {
                        Object[] ob = new Object[4];//Crea un vector para almacenar los valores del ResultSet
                        ob[0] = (consulta.getString(1));
                        ob[1] = (consulta.getString(2));
                        ob[2] = (consulta.getString(3));
                        ob[3] = (consulta.getString(4));
                        contador++;
                        //Se añade la fila al modelo
                        modelo.addRow(ob);
                        ob = null;//limpia los datos de el vector de la memoria
                    }
                
            }
            if (contador == 0) {

                JOptionPane.showMessageDialog(null, "No se encontraron usuarios con el cargo ingresado",
                        "Atención", JOptionPane.WARNING_MESSAGE);
            }
            consulta.close();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Responsable: Esteban Aguirre Martinez.
     * @return void
     * Método consulta las rutas usadas por un usuario segun su identificacion
     * y llena una tabla con la informacion
     */
    public void llenarTablaRutasFrecuentes(JTable tablaRutas,String pin)
    {
        ResultSet consulta = dao.consultarRutasFrecuentes(pin);
        DefaultTableModel modelo = new DefaultTableModel();
            
        try {

            tablaRutas.setModel(modelo);
            
            //se llenan las cabeceras de las columnas de la tabla con la informacion
            //que se recibira
            modelo.addColumn("Id ruta");
            modelo.addColumn("Nombre ruta");
            modelo.addColumn("Cantidad ingresos");
            modelo.addColumn("Recorrido");
            
            //Recorro el ResultSet que contiene los resultados.
            int contador = 0;

            //se llena la tabla con los registros de la consulta
            while (consulta.next()) {
                Object[] ob = new Object[4];//Crea un vector para almacenar los valores del ResultSet
                ob[0] = (consulta.getString(2));
                ob[1] = (consulta.getString(4));
                ob[2] = (consulta.getString(3));
                ob[3] = (consulta.getString(5));
                contador++;
                //Se añade la fila al modelo
                modelo.addRow(ob);
                ob = null;//limpia los datos de el vector de la memoria
            }
            if (contador == 0) {

                JOptionPane.showMessageDialog(null, "No se ha encontrado ningun pasajero con la tarjeta ingresada",
                        "Atención", JOptionPane.WARNING_MESSAGE);
            }
            consulta.close();
        } catch (SQLException ex) {
            System.out.println(ex);
        }

    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna las rutas que se mostraran en el reporte
     * @return ResultSet
     */
    public ResultSet consultarRutasReporte()
    {
        return dao.peticionRutasReporte();
    }
    
    
    /**
     * Resposnable: Annie Paola Muñoz Ll.
     * @return ResultSet
     * Método que retorna los conductores y placa de los buses que se le han asignado
     */
     public ResultSet consultarBusesConductores() {
        
        
        ResultSet resultado = dao.consultarBusesConductores();
        return resultado;
    }
    
    
 /**
     * Resposnable: Annie Paola Muñoz Ll.
     * @param estacion
     * @return ResultSet
     * Método que retorna el id de la estacion con el nombre que ingreso por parámetro
     */
 public String consultarEstacion(String estacion) {
ResultSet resultado = dao.consultarEstacion(estacion);
        String id = "";
        try {
            while(resultado.next()){
                
                id = resultado.getString("id_estacion").toString();
                
            }   } catch (SQLException ex) {
            Logger.getLogger(ControladorGerente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return id;    }

 
  /**
     * Resposnable: Annie Paola Muñoz Ll.
     * @param combo
     * Método que añade al combo ingresado por parámetro los años donde se han hecho solicitudes
     */
 public void consultarAhnosSolicitudes(JComboBox combo) {
        
          ResultSet result = dao.consultarAnhosSolicitudes();

        try {
            while (result.next()) {
                combo.addItem(result.getString("años"));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }

        
    }

 
 /**
     * Resposnable: Annie Paola Muñoz Ll.
     * @param mes
     * @param anio
     * @param id_estacion
     * @return ResultSet
     * Método que retorna la informacion de las solicitudes y quién las soluciono, de una
     * estación y fecha en especifico.
     */
    public ResultSet consultarSolicitudes(int mes, String anio, String estacion) {
        
        ResultSet resultado = dao.consultarSolcitudes(mes, anio, estacion);
        return resultado;
        
    }

    /**
     * Resposnable: Annie Paola Muñoz Ll.
     * @param jcb_estacion
     * Método que añade al combo ingresado por parámetro el nombre de las sedes del sistema
     */
    public void consultarEstaciones(JComboBox<String> combo_estacion) {
         ResultSet resultado = dao.consultarEstaciones();
        try {
            while(resultado.next()){
                
                combo_estacion.addItem(resultado.getString("nombre_estacion").toString());
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControladorGerente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
       
    /**
     * Resposnable: Esteban Aguirre Martinez
     * @return ResultSet
     * Método qúe retorna la informacion de las quejas comunes para el reporte.
     */
    public ResultSet consultarQuejasComunes() {
        
        ResultSet resultado = dao.consultarSolcitudes();
        return resultado;
        
    }
    
    
    /**
     * Resposnable: Esteban Aguirre Martinez
     * @return ResultSet
     * Método que retorna la informacion de los pasajeros modificados segun un mes y año.
     */
    public ResultSet consultarPasajerosMovilizados(int mes,String anio)
    {
        ResultSet resultado = dao.consultarPasajerosMovilizados(mes, anio);
        return resultado;
    }
    
    /**
     * Resposnable: Esteban Aguirre Martinez
     * @return ArrayList
     * Método que retorna un array con los años presentes en los ingresos
     */
    public ArrayList consultarAniosIngresos()
    {
        ResultSet result = dao.consultarAniosIngresos();

        ArrayList anhos = new ArrayList();

        try {
            while (result.next()) {
                anhos.add(result.getString(1));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }

        return anhos;
    }
}
