/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
import accesoDatos.DaoLogin;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
 *
 * @author sara
 */
public class ControladorLogin {
     DaoLogin dao;
    
    public ControladorLogin(){
       dao  = new DaoLogin(); 
    }
    
   
    
    /**
     * Responsable: Jhon Frederick Mena R
     *
     * @param usuario
     * @param contraseña
     * @return Vector<String>
     */
    public Vector<String> consultarCargo(String usuario, String contraseña) {
        ResultSet respuesta = dao.peticionCargo(usuario, contraseña);
        Vector<String> valores = new Vector<String>();
        try {
            if (respuesta.next()) {
                for (int iterador = 1; iterador <= respuesta.getMetaData().getColumnCount(); iterador++) {
                    String valor = respuesta.getString(iterador);
                    valores.add(valor);
                }
            }

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al extraer los datos de la consulta\n" + sqlex, "Error consulta", JOptionPane.ERROR_MESSAGE);
        }
        return valores;
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param id
     * @return String
     * Método que consulta el nombre de un empleado
     */
    public String consultarNombre(String id) {
        ResultSet respuesta = dao.peticionNombre(id);
        String nombre="";
        try {
            if (respuesta.next()) {
                nombre = respuesta.getString(1) + " " + respuesta.getString(2);
            }

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al extraer los datos de la consulta\n" + sqlex, "Error consulta", JOptionPane.ERROR_MESSAGE);
        }
        
        System.out.println("nom"+nombre);
        return nombre;
    }
    
}
