/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import accesoDatos.DaoDirectorOperativo;
import java.io.FileInputStream;


/**
 *
 * @author sara
 */
public class ControladorDirectorOperativo {

    DaoDirectorOperativo dao;

    public ControladorDirectorOperativo() {
        dao = new DaoDirectorOperativo();
    }

    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @param placa
     * @param tipo
     * @param estado
     * @return boolean Método que recibe los datos de la interfaz y lo comunica
     * con el respectivo método para agregar un bus a la base de datos. Retorna
     * un booleano que representa el exito en la operación
     */
    public boolean agregarBus(String placa, String tipo, String estado, String idRuta) {
        boolean resultado = dao.registrarBus(placa, tipo, estado, idRuta);
        return resultado;
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H
     * @param nombre
     * @return int
     * Metodo que verifica si una ruta existe
     */
    public int verificarRutaExiste(String nombre) {
        int count = -1;
        
        ResultSet resultado = dao.peticionRutaExiste(nombre);
        
        try {
            resultado.next();
            
            count = resultado.getInt(1);
            
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        
        return count;
    }
    

    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @param placa
     * @return String [] Método que toma la placa de la interfaz y la conecta
     * con el metodo para consultar la informacion respectiva de un bus,
     * posteriormente se toma la informacion del ResultSet y se retorna en un
     * arreglo
     */
    public ArrayList consultarBus(String placa) {
        ResultSet resultado = dao.consultarBus(placa);
        ArrayList respuesta = new ArrayList();

        try {
            while (resultado.next()) {

                respuesta.add(resultado.getString(1));
                respuesta.add(resultado.getString(2));
                respuesta.add(resultado.getString(3));
                respuesta.add(resultado.getString(4));
                

            }
            resultado.close();
        } catch (SQLException sql) {
            System.out.println(sql.getMessage());
        }

        return respuesta;

    }

    /**
     * Resultado: Sara Catalina Muñoz H.
     *
     * @param placa
     * @param estado
     * @return boolean Método que recibe los datos de la interfaz y lo comunica
     * con el respectivo metodo para realizar la peticion de actualizar el
     * estado de un bus. Se retorna un booleano que representa el éxito de la
     * operacion
     */
    public boolean modificarBus(String placa, String estado, String idRuta) {
        boolean resultado = dao.modificarBus(placa, estado, idRuta);
        return resultado;
    }

    /**
     * Responsable: Sara Catalina Muñoz H
     * @param placa
     * @return int
     * Mpetodo que verifica que un bus existe
     */
    public int verificarBusExiste(String placa){
        ResultSet resultado = dao.peticionBusExiste(placa);
        int retorno  =  -1;
        try {
            resultado.next();
            retorno = resultado.getInt(1);           
            
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        
        return retorno;
    }
    
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @param placa
     * @return boolean Método que recibe los datos de la interfaz y lo comunica
     * con el respectivo metodo para realizar la peticion de eliminar un bus. Se
     * retorna un booleano que representa el éxito de la operacion
     */
    public boolean eliminarBus(String placa) {

        boolean retornoTurno = dao.eliminarTurno(placa);

        boolean retornoBus = dao.eliminarBus(placa);

        boolean retorno = retornoBus && retornoTurno;
        return retorno;
    }

    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @return ArrayList Método que retorna en un arrayList los nombres de las
     * estaciones en el sistema
     */
    public ArrayList consultarNombreEstaciones() {
        ResultSet resultado = dao.consultarEstaciones();
        ArrayList respuesta = new ArrayList();

        try {
            while (resultado.next()) {

                respuesta.add(resultado.getString(1));

            }
            resultado.close();
        } catch (SQLException sql) {
            System.out.println(sql.getMessage());
        }
        return respuesta;
    }

    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @param nombre
     * @param recorrido
     * @param estado
     * @param fis
     * @param longitudBytes
     * @return boolean
     * Método que ingresa una ruta con imagen
     */
    public boolean agregarRuta(String nombre, String recorrido, String estado, FileInputStream fis,int longitudBytes) 
    {
        boolean resultado = dao.agregarRuta(nombre, recorrido, estado, fis, longitudBytes);         
        return resultado;
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param nombre
     * @param recorrido
     * @param estado
     * @return boolean
     * Método que ingresa una ruta sin imagen
     */
    public boolean agregarRutaSinImagen(String nombre, String recorrido, String estado) 
    {
        boolean resultado = dao.agregarRutaSinImagen(nombre, recorrido, estado);         
        return resultado;
    }
    

    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @param nombre
     * @return
     */
    public String consultarIdRuta(String nombre) {
        ResultSet resultado = dao.consultarIdRuta(nombre);
        String id = "";
        try {
            resultado.next();
            id = resultado.getString(1);
        } catch (SQLException sql) {
            System.out.println(sql);
        }
        return id;
    }

    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @param nombre
     * @return String
     */
    public String consultarIdEstacion(String nombre) {
        ResultSet resultado = dao.consultarIdEstacion(nombre);
        String id = "";
        try {
            resultado.next();
            id = resultado.getString(1);
        } catch (SQLException sql) {
            System.out.println(sql);
        }
        return id;
    }

    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @return ArrayList
     */
    public ArrayList consultarNombreRutas() {
        ResultSet resultado = dao.consultarRutas();

        ArrayList respuesta = new ArrayList();

        try {
            while (resultado.next()) {

                respuesta.add(resultado.getString(1));

            }
            resultado.close();
        } catch (SQLException sql) {
            System.out.println(sql.getMessage());
        }
        return respuesta;

    }

    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @param ruta
     * @param estacion
     * @return
     */
    public boolean agregarRutasPasan(String ruta, String estacion) {
        boolean resultado = dao.agregarRutasPasan(estacion, ruta);
        return resultado;
    }

    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @param fecha
     * @param turno
     * @return ArrayList Método que construye un arreglo con los conductores
     * disponibles en cierto turno
     */
    public ArrayList conductorDisponible(String fecha, String turno) {

        ResultSet resultado = dao.consultarConductoresDisponibles(turno, fecha);

        ArrayList respuesta = new ArrayList();

        try {
            while (resultado.next()) {

                respuesta.add(resultado.getString(1));

            }
            resultado.close();
        } catch (SQLException sql) {
            System.out.println(sql.getMessage());
        }
        return respuesta;

    }

    /**
     * Responsable: Sara Catalina Muñoz H.
     * @return ArrayList
     * Método que construye un arreglo con las placas de los buses
     */
    public ArrayList consultarBusesDisponibles(String horario, String fecha) {

        ResultSet resultado = dao.consultarBusesDisponibles(horario, fecha);

        ArrayList respuesta = new ArrayList();

        try {
            while (resultado.next()) {

                respuesta.add(resultado.getString(1));

            }
            resultado.close();
        } catch (SQLException sql) {
            System.out.println(sql.getMessage());
        }
        return respuesta;

    }
    
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @param placa
     * @return String Método que consulta el nombre de una ruta que sigue cierto
     * bus
     */
    public String consultarRuta(String placa) {

        ResultSet rutas = dao.consultarIDRuta(placa);
        String nombreRuta = "";
        String idRuta = "";
        try {
            while (rutas.next()) {
                idRuta = rutas.getString(1);
            }
            rutas.close();

            ResultSet nombres = dao.consultarNombreRuta(idRuta);
            nombres.next();
            nombreRuta = nombres.getString(1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        }

        return nombreRuta;
    }

    
    
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param id
     * @return String
     * Método que consulta elnombre de un empleado segun su id
     */
    public String consultarNombreEmplado(String id) {

        ResultSet nombres = dao.consultarNombreEmpleado(id);
        String nombre = "";
      
        try {
            while (nombres.next()) {
                nombre = nombres.getString(1)+" "+nombres.getString(2);
            }
            nombres.close();

           
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        }

        return nombre;
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param empleado
     * @param placa
     * @param fecha
     * @param horario
     * @return boolean
     * Método que inserta un registro en la tabla de turnos
     */
    public boolean registrarTurno(String empleado, String placa, String fecha, String horario) {

        boolean resultado = dao.registrarTurno(empleado, placa, fecha, horario);
        return resultado;
    }

    
    public String consultarRutadeBus(String placa){
        String ruta = "";
        
        ResultSet resultado = dao.consultarRutadeBus(placa);
        try {
            resultado.next();
            ruta = resultado.getString(1);            
            
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        
        return ruta;
        
    }
    
    
    public ResultSet consultarTurno(String id, String fecha) {
        ResultSet resultado = dao.consultarTuno(id, fecha);
        return resultado;
    }

    /**
     * Responsable: Jhon Frederick Mena R 
     * Metodo que retorna una matriz de objetos que representan 
     * la informacion de la consulta
     * @param modeloTabla
     */
    public void consultarRutas(DefaultTableModel modeloTabla) {
        ResultSet respuesta = dao.peticionConsultarRutas();
        try {
            int numColumns = respuesta.getMetaData().getColumnCount();
            while (respuesta.next()) {
                Object[] fila = new Object[numColumns];
                fila[0] = respuesta.getString(1);
                fila[1] = respuesta.getString(2);
                fila[2] = respuesta.getString(3);
                fila[3] = respuesta.getString(4);
                if(respuesta.getObject(5) == null)
                {
                    fila[4] = "No disponible";
                }else
                {
                    fila[4] = "Disponible";
                }               
                modeloTabla.addRow(fila);
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al extraer los datos de la consulta\n" + sqlex,
                    "Error consulta", JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * Responsable: Jhon Frederick Mena R 
     * Metodo que retorna un booleano si la insercion se hizo o no 
     * con exito
     * @param ubicacion
     * @param nomb_estacion
     * @param id_empleado
     * @return boolean
     */
    public boolean ingresarEstaciones(String ubicacion, String nomb_estacion, String id_empleado) 
    {
        //Determina si la insercion se hizo con exito
        if (1 == dao.peticionIngresarEstacion(nomb_estacion, ubicacion, id_empleado)) 
        {
            return true;
        } else 
        {
            return false;
        }

    }
    
    /**
     * Responsable: Jhon Frederick Mena R 
     * Metodo que le añade los empleados al modelo de la tabla
     * @param modeloTabla 
     */
    public void consultarEmpleadosDESinEstacion(DefaultTableModel modeloTabla)
    {
        ResultSet consulta = dao.peticionEmpleadosDESinEstacion();
        try 
        {
            int numColumns = consulta.getMetaData().getColumnCount();
            while (consulta.next()) {
                Object[] fila = new Object[numColumns];
                fila[0] = consulta.getString(1);
                fila[1] = consulta.getString(2);
                //Se añade la fila al modelo de la tabla
                modeloTabla.addRow(fila);

            }
            
        } catch (SQLException sqlex) 
        {
            JOptionPane.showMessageDialog(null, "Error al extraer los datos de los empleados \n" + sqlex,
                    "Error consulta", JOptionPane.ERROR_MESSAGE);
        }
        
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna  un booleano que indica si la asignacion de la imagen fue
     * exitosa
     * @param codigoRuta
     * @param fis
     * @param longitudBytes
     * @return boolean
     */
    public boolean asignarImagenRuta(String codigoRuta,FileInputStream fis,int longitudBytes)
    {
        int resultado = dao.peticionAsignarImagenRuta(codigoRuta,fis,longitudBytes);
        if(resultado == 1)
        {
            return true;
        }else
        {
            return false;
        }
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que modifica la informacion de una ruta
     * @param codigoRuta
     * @param nombreRuta
     * @param estado
     * @param descripcion
     * @return boolean
     */
    public boolean modificarRuta(String codigoRuta,String nombreRuta,String estado,String descripcion)
    {
        int resultado = dao.peticionModificarRuta(codigoRuta, nombreRuta, estado, descripcion);
        if(resultado == 1)
        {
            return true;
        }else
        {
            return false;
        }
    }
 
}
