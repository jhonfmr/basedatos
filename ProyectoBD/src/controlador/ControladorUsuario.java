/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import accesoDatos.DaoUsuario;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.sql.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author jhonfmr
 */
public class ControladorUsuario 
{
    DaoUsuario accesoDatos;

    public ControladorUsuario()
    {
        accesoDatos = new DaoUsuario();
    }
    
     /**
     * Responsable:Jhon Frederick Mena R
     * Metodo que retorna un booleano indicando si la tarjeta esta o no en el sistema
     * @param pin_tarjeta
     * @return boolean
     */
    public boolean verificarPin(int pin_tarjeta) 
    {
        ResultSet consulta = accesoDatos.peticionPinTarjeta(pin_tarjeta);
        try 
        {
            if(consulta.next())
            {
                return true;
            }
            
        } catch (Exception sqlex) 
        {
            JOptionPane.showMessageDialog(null,"Error al obtener el pin de la tarjeta de la BD \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    /**
     * Responsable:Jhon Frederick Mena R
     * Metodo que retorna si la cedula a quien pertenece la tarjeta, si no le pertenece a nadie
     * retorna un string vacio
     * @param pin_tarjeta
     * @return String
     */
    public String retornarPasajero(int pin_tarjeta) 
    {
        ResultSet consulta = accesoDatos.consultaPasajero(pin_tarjeta);
        String valor = "";
        try 
        {           
            if(consulta.next())
            {
                valor = consulta.getString(1);
            }
        } catch (SQLException sqlex) 
        {
            JOptionPane.showMessageDialog(null,"Error al obtener la cedula del pasajero \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return valor;
    }
    
    /**
     * Responsable:Jhon Frederick Mena R
     * Metodo que permite simular el ingres de un usuario al sistema
     * @param id_pasajero
     * @param fecha
     * @param hora
     * @param id_ruta
     * @return int
     */
    public boolean ingresarSistema(String id_pasajero,int id_ruta,String fecha,String hora)
    {
        if(accesoDatos.peticionIngresarSistema(id_pasajero, id_ruta, fecha, hora)  == 1)
        {
            return true;
        }else
        {
            return false;
        }
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que rellena el modelo de un comboBox con elementos
     * @param modelo 
     */
    public void rellenarRutas(DefaultComboBoxModel<String> modelo)
    {
        try {
            ResultSet consulta = accesoDatos.peticionConsultarRutas();
            while(consulta.next())
            {
                String elemento = consulta.getString(2);
                modelo.addElement(elemento);
            }
        } catch (SQLException sqlex) 
        {
            JOptionPane.showMessageDialog(null,"Error al obtener las rutas del sistema \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que rellena el modelo de una tabla con elementos
     * @param modeloTabla 
     */
    public void rellenarRutas(DefaultTableModel modeloTabla)
    {
        try {
            ResultSet consulta = accesoDatos.peticionConsultarRutas();
            int numeroColum = consulta.getMetaData().getColumnCount();
            while(consulta.next())
            {
                Object[] fila = new Object[numeroColum];
                fila[0] = consulta.getString(2);
                fila[1] = consulta.getString(3);
                //Se añade el arreglo con la informacion a la tabla
                modeloTabla.addRow(fila);
            }
        } catch (SQLException sqlex) 
        {
            JOptionPane.showMessageDialog(null,"Error al obtener las rutas del sistema \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
    }  
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Retorna el valor de la tarjeta que indica si 
     * @param pin_tarjeta
     * @return int
     */
    public int retornarSaldo(String pin_tarjeta)
    {
        ResultSet consulta =  accesoDatos.peticionRetornaSaldo(pin_tarjeta);
        int saldo = -1;
        try 
        {
            if(consulta.next())
            {
               saldo = Integer.parseInt(consulta.getString(1)); 
            }                      
        } catch(SQLException sqlex) 
        {
            JOptionPane.showMessageDialog(null,"Error al obtener el saldo de la tarjeta \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return saldo;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que rellena un combo con el nombre de las estaciones de la base de datos
     * @param modeloCombo 
     */
    public void rellenarEstaciones(DefaultComboBoxModel<String> modeloCombo)
    {
        ResultSet consulta = accesoDatos.peticionConsultarNombreEstaciones();
        try 
        {
            while(consulta.next())
            {
                String estacion = consulta.getString(1);
                modeloCombo.addElement(estacion);
            }
        } catch (SQLException sqlex) 
        {
            JOptionPane.showMessageDialog(null,"Error al obtener las estaciones del sistema \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que devuelve las rutas que pasan por cierta estacion
     * @param nombreEstacion 
     * @param modeloTabla 
     */
    public void buscarRutas(String nombreEstacion,DefaultTableModel modeloTabla)
    {
        ResultSet consulta = accesoDatos.peticionBuscarRutas(nombreEstacion);
        try 
        {
            int numeroColum = consulta.getMetaData().getColumnCount();
            while(consulta.next())
            {
                Object[] fila = new Object[numeroColum];
                fila[0] = consulta.getString(1);
                fila[1] = consulta.getString(2);
                //Se añade el arreglo con la informacion a la tabla
                modeloTabla.addRow(fila);
            }
        } catch (SQLException sqlex) 
        {
            JOptionPane.showMessageDialog(null,"Error al obtener las rutas que pasan por la estacion " 
                    + nombreEstacion + "\n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna las rutas que pasan por un par de estaciones determinadas, una es el origen
     * el otro es el destino
     * @param estacionOrigen
     * @param estacionDestino
     * @param modeloTabla 
     */
    public void buscarRutas(String estacionOrigen,String estacionDestino,DefaultTableModel modeloTabla)
    {
        ResultSet consulta = accesoDatos.peticionRutasOrigenDestino(estacionOrigen, estacionDestino);
        try 
        {
            int numeroColum = consulta.getMetaData().getColumnCount();
            while (consulta.next()) 
            {
                Object[] fila = new Object[numeroColum];
                fila[0] = consulta.getString(1);
                fila[1] = consulta.getString(2);
                //Se añade el arreglo con la informacion a la tabla
                modeloTabla.addRow(fila);
            }
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null, "Error al obtener las rutas" + sqlex,
                    "Error SQL", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna toda la informacion asociada con un numero de solicitud de queja
     * o reclamo
     * @param n_tiquete 
     * @return  String
     */
    public String buscarSolicitud(int n_tiquete)
    {
        ResultSet consulta = accesoDatos.peticionBuscarSolicitud(n_tiquete);
        ResultSet medidasSolucion = accesoDatos.peticionBuscarMedidasSolucion(n_tiquete);
        String informacionSolicitud = "";
        String n_tiquet = "";
        String motivo = "";
        String fecha_Sol = "";
        String descripcion_Sol= "";
        String estado_Sol = "";
        String tipo_Queja = "";
        String id_pasajero = "";
        String nombre_pasajero = "";
        try 
        {
            if (consulta.next()) 
            {
                //Se toman los datos de la solicitud
                n_tiquet = consulta.getString("n_tiquete");
                motivo = consulta.getString("motivo_solicitud");
                fecha_Sol = consulta.getString("fecha_solicitud");
                descripcion_Sol = consulta.getString("descripcion_Solicitud");
                estado_Sol = consulta.getString("estado_solicitud");
                tipo_Queja = consulta.getString("tipo_queja");
                id_pasajero = consulta.getString("id_pasajero");
                nombre_pasajero = consulta.getString("nombre_pasajero");
            }
        } catch (SQLException sqlex) 
        {
            JOptionPane.showMessageDialog(null, "Error al obtener la solicitud\n" + sqlex,
                    "Error SQL", JOptionPane.ERROR_MESSAGE);
        }
        //Se da un formato al string que se mostrara en el are de texto
        informacionSolicitud = "No. tiquete: " + n_tiquet + "\t\t Fecha: " + fecha_Sol + "\n"
                + "Motivo Solicitud: " + motivo + "\t\t Tipo Queja/Reclamo: " + tipo_Queja + "\n"
                + "Cedula: " + id_pasajero + "\t\t Nombre: " + nombre_pasajero.toUpperCase() + "\n"
                + "Estado: " + estado_Sol + "\n\n"
                + "Descripcion Solicitud: \n"
                + "\t" + descripcion_Sol + "\n\n"
                + "Medidas Solución:" + "\n";
        try 
        {
            //Se toman las medidas de solucion que tenga la solicitud de queja/Reclamo
            while (medidasSolucion.next()) 
            {
                informacionSolicitud += "\t" + medidasSolucion.getString("medida_solucion") + "\n";
            }
        } catch (SQLException sqlex) 
        {
            JOptionPane.showMessageDialog(null, "Error al obtener laS medidas de solucion \n" + sqlex,
                    "Error SQL", JOptionPane.ERROR_MESSAGE);
        }
        
        
        return informacionSolicitud;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna la imagen de recorrido de una ruta
     * @param nombreRuta
     * @return Image 
     */
    public ImageIcon consultarImagenRuta(String nombreRuta)
    {
        //Se toma la ruta donde esta ubbicada la imagen
        URL ruta = accesoDatos.peticionConsultarRutaImagen(nombreRuta);
        ImageIcon imagenRuta= new ImageIcon(ruta);
        return imagenRuta;
    }
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna la imagen de recorrido de una ruta
     * @param nombreRuta
     * @return Image 
     */
    public ImageIcon consultarImagen(String nombreRuta)
    {
        //Se toma la ruta donde esta ubbicada la imagen
        ResultSet consulta = accesoDatos.peticionConsultarImagen(nombreRuta);
        InputStream is;
        ImageIcon imagenRuta = null;
        try 
        {
            if(consulta.next())
            {
                is = consulta.getBinaryStream(1);
                if(is != null)
                {
                    BufferedImage bi = ImageIO.read(is);
                    imagenRuta = new ImageIcon(bi);
                }             
            }
        } catch (SQLException sqlex) 
        {
            JOptionPane.showMessageDialog(null, "Error al obtener la imagen \n" + sqlex,
                    "Error SQL", JOptionPane.ERROR_MESSAGE);
        }catch (IOException ioex)
        {
            JOptionPane.showMessageDialog(null, "Error al crear el buffer de la imagen \n" + ioex,
                    "Error SQL", JOptionPane.ERROR_MESSAGE);
        } 
        return imagenRuta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que descuenta de una tarjeta el pasaje
     * @param pin_tarjeta
     * @return String
     */
    public String descontarSaldo(String pin_tarjeta)
    {
        int exitoResta = 0;
        exitoResta = accesoDatos.peticionRestarSaldo(pin_tarjeta);
        String saldo = "";
        if(exitoResta  == 1)
        {
            ResultSet consulta = accesoDatos.peticionRetornaSaldo(pin_tarjeta);
            try 
            {
                if(consulta.next())
                {
                    saldo = consulta.getString(1);
                }
                return saldo;
            } catch (SQLException sqlex) 
            {
                JOptionPane.showMessageDialog(null, "Error al descontar el saldo de la tarjeta \n" + sqlex,
                        "Error SQL", JOptionPane.ERROR_MESSAGE);
            }
        }
        {
            return saldo;
        }
    }
}
