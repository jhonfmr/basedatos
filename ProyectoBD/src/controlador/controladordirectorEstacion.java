/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import accesoDatos.DaoDirectorEstacion;
import java.sql.*;
import java.util.HashMap;
import javax.swing.*;
import javax.swing.table.*;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author desarrollo
 */
public class controladordirectorEstacion {

    DaoDirectorEstacion Daodirector = new DaoDirectorEstacion();

    /**
     * Responsable: Esteban Aguirre Martinez
     *
     * @param tabla_reclamo
     * @param restriccion
     * @param ntiquete
     * @param modelo_tabla
     * @return void 
     * Metodo actualiza el modelo de la tabla segun la informacion
     * de una consulta
     */
    public void consultarQuejaTiquete(JTable tabla_reclamo, String restriccion, String ntiquete, DefaultTableModel modeloTabla) {

        ResultSet respuesta = Daodirector.consultarQuejas(restriccion, ntiquete);
        tabla_reclamo.setModel(modeloTabla);

        try {

            modeloTabla.addColumn("numero tiquete");
            modeloTabla.addColumn("id pasajero");
            modeloTabla.addColumn("tipo queja");
            modeloTabla.addColumn("motivo");
            modeloTabla.addColumn("fecha solicitud");
            modeloTabla.addColumn("estado");

            //Recorro el ResultSet que contiene los resultados.         
            while (respuesta.next()) {
                Object[] ob = new Object[7];//Crea un vector para almacenar los valores del ResultSet
                ob[0] = (respuesta.getString(1));
                ob[1] = (respuesta.getString(2));
                ob[2] = (respuesta.getString(3));
                ob[3] = (respuesta.getString(4));
                ob[4] = (respuesta.getString(5));
                ob[5] = (respuesta.getString(6));

                //añado el modelo a la tabla
                modeloTabla.addRow(ob);

                ob = null;//limpia los datos de el vector de la memoria
            }
            respuesta.close();
            //Cierra el ResultSet
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Responsable: Esteban Aguirre Martinez
     *
     * @param ntiquete
     * @param solucion
     * @return void 
     * Metodo envia informacion para insertar en la tabla de
     * soluciones
     */
    public void solucionarQuejaTiquete(String ntiquete, String solucion) {
        boolean exitoso = Daodirector.resolverQuejas(ntiquete, solucion);

        if (exitoso) {
            JOptionPane.showMessageDialog(null, "Medida de solucion añadida con exito");
        } else {
            JOptionPane.showMessageDialog(null, "Se presento un fallo al añadir la medida de solucion");
        }

    }

    /**
     * Responsable: Esteban Aguirre Martinez
     *
     * @param ntiquete
     * @param txt_tipo
     * @param txt_motivo
     * @param txt_fecha
     * @param txt_estado
     * @return boolean
     * Metodo que busca un tiquete y actualiza los datos de la pantalla, luego
     * retorna true o false dependiendo de si encontro informacion o no
     */
    public boolean buscarTiquete(String ntiquete, JTextField txt_tipo, JTextField txt_motivo, JTextField txt_fecha, JTextField txt_estado) {
        ResultSet existe = Daodirector.existeTiquete(ntiquete);
        try {
            if (existe.next()) {
                txt_tipo.setText(existe.getString(1));
                txt_motivo.setText(existe.getString(2));
                txt_fecha.setText(existe.getString(3));
                txt_estado.setText(existe.getString(4));
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return false;
    }

    /**
     * Responsable: Esteban Aguirre Martinez
     *
     * @param estado
     * @param codigo
     * @return String
     * Metodo que retorna un mensaje de correcto o incorrecto dependiendo de
     * si se pudo modificar el estado o no
     */
    public String peticionModificarEstadoReclamo(String estado, String codigo) {
        String retorno = Daodirector.peticionModificarEstadoReclamo(estado, codigo);

        return retorno;
    }

    /**
     * Responsable: Esteban Aguirre Martinez
     *
     * @param codigoReclamo
     * @return String
     * Metodo que retorna la informacion de un ticket segun su codigo
     */
    public ResultSet consultaInformacionTicket(String codigoReclamo) {
        ResultSet consulta = Daodirector.existeTiquete(codigoReclamo);

        return consulta;
    }

}
