/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import accesoDatos.DaoAuxiliar;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import javax.swing.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author annie
 */
public class ControladorAuxiliar {
    String costo_tarjeta;
    DaoAuxiliar auxiliar = new DaoAuxiliar();
    ResultSet resultado;
    //Se declaran las variables globales a usar
    Calendar date = new GregorianCalendar();
                        int año = date.get(Calendar.YEAR);
                        int mes = date.get(Calendar.MONTH);
                        int dia = date.get(Calendar.DAY_OF_MONTH);
                        String fecha = año + "-" + (mes + 1) + "-" + dia;
    
    int hora =date.get(Calendar.HOUR_OF_DAY);
    int minutos = date.get(Calendar.MINUTE);
    int segundos = date.get(Calendar.SECOND);
    
    String hora_hoy = hora+":"+minutos+":"+segundos;
    
    
     /**
     * Responsable: Annie Paola Muñoz
     * @param saldo_txt
     * @param saldo
     * @param id_empleado
     * @param estacion
     * @param combo
     * Método que realiza la venta de una tarjeta en el sistema. Realiza la recarga de la venta y registra
     * la tarjeta.
     */
    public void venderTarjeta(JTextField saldo_txt, String saldo, String id_empleado, String estacion, JComboBox combo){
        
        costo_tarjeta = "3000";
        
        
        int recarga = Integer.parseInt(saldo);
        String pin ="";
        
        if(auxiliar.agregarTarjeta(saldo, estacion) == 0){                  
                JOptionPane.showMessageDialog(null, "La venta no se pudo realizar exitosamente",
                        null, JOptionPane.WARNING_MESSAGE);                        
        }else {
                resultado = auxiliar.consultarPin();               
                
        try {
            while(resultado.next()){
                pin = resultado.getString("pin");        
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControladorAuxiliar.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        
        }if(auxiliar.venderTarjeta(pin, id_empleado, estacion, costo_tarjeta, fecha) == 0){                    
                    JOptionPane.showMessageDialog(null, "La venta no se pudo realizar exitosamente",
                        null, JOptionPane.WARNING_MESSAGE);   
                    
        }else if(auxiliar.ingresarRecarga(estacion, pin, recarga, fecha, hora_hoy) == 0){
                 
                 JOptionPane.showMessageDialog(null, "No se pudo realizar la recarga",
                         null, JOptionPane.WARNING_MESSAGE);
                 
             }else{
                 
                 JOptionPane.showMessageDialog(null, "Venta realizada con éxito.",
                         null, JOptionPane.WARNING_MESSAGE);
                 saldo_txt.setText("");
                 combo.setSelectedIndex(0);
                         
             }
        
        
    
    }
    
     /**
     * Responsable: Annie Paola Muñoz
     * @param recarga_txt
     * @param pin_txt
     * @param combo
     * @param pin
     * @param recargar
     * @param id_estacion
     * Método que realiza la recarga de una tarjeta. Modifica el saldo de una tarjeta en el sistema.
     */
    public void recargarTarjeta(JTextField recarga_txt, JTextField pin_txt, JComboBox combo, String pin, String recargar, String id_estacion){
        
        
        int recarga = Integer.parseInt(recargar);
        boolean vacio = true;
        
         try {
             resultado = auxiliar.consultarSaldo(pin);
             int saldo_viejo = 0;
             while(resultado.next()){
                 
                 saldo_viejo = Integer.parseInt(resultado.getString("saldo"));                 
                 vacio = false;
             }             
             
             int saldo_nuevo = recarga + saldo_viejo;
             
             if(vacio){
             
             JOptionPane.showMessageDialog(null, "No se encontro ningúna tarjeta con ese pin.",
                         null, JOptionPane.WARNING_MESSAGE);
                    pin_txt.setText("");
             
             }else if(auxiliar.recargarTarjeta(pin, saldo_nuevo) == 0){
                 
                 JOptionPane.showMessageDialog(null, "Tarjeta no recargada con éxito.",
                         null, JOptionPane.WARNING_MESSAGE);
                 pin_txt.setText("");
                 
             }else if (auxiliar.ingresarRecarga(id_estacion, pin, recarga, fecha, hora_hoy) == 0 ){
                 
                 JOptionPane.showMessageDialog(null, "Recarga no ingresada con éxito",
                         null, JOptionPane.WARNING_MESSAGE);
                 pin_txt.setText("");
                         
             }else{
             JOptionPane.showMessageDialog(null, "Recarga realizada con éxito.",
                         null, JOptionPane.WARNING_MESSAGE);
             recarga_txt.setText("");
              pin_txt.setText("");
              combo.setSelectedIndex(0);
             }
         
         } catch (SQLException ex) {
             Logger.getLogger(ControladorAuxiliar.class.getName()).log(Level.SEVERE, null, ex);
         }
        
        
    
    }

    /**
     * Responsable: Annie Paola Muñoz
     * @param jtxt_cedula
     * @param jtxt_pin
     * @param jtxt_nombre
     * @param jtxt_apellido
     * @param jtxt_telefono
     * @param nombre
     * @param apellido
     * @param telefono
     * @param pin
     * @param cedula
     * Método que personaliza una tarjeta modificando su tipo a personal e ingresando el nuevo pasajero que
     * personalizo la tarjeta en el sistema.
     */
    public void personalizarTarjeta(JTextField jtxt_cedula, JTextField jtxt_pin, JTextField jtxt_nombre, JTextField jtxt_apellido,
                             JTextField jtxt_telefono, String nombre, String apellido, String telefono, String pin, String cedula) {
        

        
         if(auxiliar.personalizarTarjeta(pin) == 0){
        
        JOptionPane.showMessageDialog(null, "Por favor verificar el pin de la tarjeta.",
                         null, JOptionPane.WARNING_MESSAGE);
        jtxt_pin.setText("");
                 
             }else if(auxiliar.agregarPasajero(nombre, apellido, telefono, pin, cedula) == 0){
                 
                 JOptionPane.showMessageDialog(null, "Por favor verificar el pin de la tarjeta",
                         null, JOptionPane.WARNING_MESSAGE);
                 jtxt_pin.setText("");
                         
             }else{
             JOptionPane.showMessageDialog(null, "Tarjeta personalizada con éxito.",
                         null, JOptionPane.WARNING_MESSAGE);
             jtxt_pin.setText("");
             jtxt_nombre.setText("");
             jtxt_apellido.setText("");
             jtxt_telefono.setText("");
             jtxt_cedula.setText("");
             
             } 
        
        
        
    }

    /**
     * Responsable: Annie Paola Muñoz
     * @param boton
     * @param jtxt_cedula
     * @param jtxt_pin
     * @param jtxt_nombre
     * @param jtxt_apellido
     * @param jtxt_telefono
     * @param nombre
     * @param apellido
     * @param telefono
     * @param pin
     * @param cedula
     * Método que modifica la información del pasajero dueño de una tarjeta en especifico.
     */
    public void modificarTarjeta(JButton boton, JTextField jtxt_cedula, JTextField jtxt_pin, JTextField jtxt_nombre, JTextField jtxt_apellido,
                        JTextField jtxt_telefono, String nombre, String apellido, String telefono, String pin, String cedula) {
                                
       
        
        
        if(auxiliar.modificarDatosPasajero(nombre, apellido, telefono, pin, cedula) == 0){
        
        JOptionPane.showMessageDialog(null, "No se pudo personalizar la tarjeta",
                         null, JOptionPane.WARNING_MESSAGE);
                 
             }else{
                 
                 JOptionPane.showMessageDialog(null, "Se personalizo la tarjeta con èxito",
                         null, JOptionPane.WARNING_MESSAGE);
                         
             }
        
        
        
        boton.setEnabled(true);
        jtxt_pin.setEditable(true);
        jtxt_pin.setText("");
        jtxt_nombre.setText("");
        jtxt_apellido.setText("");
        jtxt_telefono.setText("");
        jtxt_cedula.setText("");
        
        
    }

    /**
     * Responsable: Annie Paola Muñoz
     * @param boton
     * @param jtxt_cedula
     * @param jtxt_nombre
     * @param jtxt_apellido
     * @param jtxt_telefono
     * @param jtxt_pin
     * @param pin
     * Método que obtiene la informacion de un pasajero en especifico del sistema que tiene una tarjeta
     * con un pin en especifico.
     */
    public void consultarInfoPasajero(JButton boton, JTextField jtxt_cedula, JTextField jtxt_nombre, JTextField jtxt_apellido, JTextField jtxt_telefono, JTextField jtxt_pin, String pin) {
     
        boolean vacio = true;
        
        try {
             
             resultado = auxiliar.consultarInfoPasajero(pin);
             
             while(resultado.next()){
                 
                 jtxt_cedula.setText(resultado.getString("id_pasajero"));
                 jtxt_nombre.setText(resultado.getString("nombre_pasajero"));
                 jtxt_apellido.setText(resultado.getString("apellido_pasajero"));
                 jtxt_telefono.setText(resultado.getString("telefono_pasajero"));
                 vacio = false;
                 
             }
         
         } catch (SQLException ex) {
             Logger.getLogger(ControladorAuxiliar.class.getName()).log(Level.SEVERE, null, ex);
         }
        
        if(vacio){
            
            JOptionPane.showMessageDialog(null, "No se encontro ningúna tarjeta con ese pin.",
                         null, JOptionPane.WARNING_MESSAGE);
        jtxt_pin.setText("");
        
        }else{
                        
        boton.setEnabled(false);
        jtxt_pin.setEditable(false);
        
        
        }
        
        
    }

    /**
     * Responsable: Annie Paola Muñoz
     * @param jtxt_pin
     * @param jtxt_saldo
     * @param pin
     * Método que obtiene el saldo de una tarejeta con un pin en especifico.
     */
    public  void consultarSaldo(JTextField jtxt_pin, JTextField jtxt_saldo, String pin) {
        String saldo = "";
        boolean vacio = true;
        try {            
             resultado = auxiliar.consultarSaldo(pin);
             
             while(resultado.next()){
                 
                 saldo = resultado.getString("saldo");
                 vacio = false;
             }} catch (SQLException ex) {
             Logger.getLogger(ControladorAuxiliar.class.getName()).log(Level.SEVERE, null, ex);
         }
        
        if(vacio){
       
            JOptionPane.showMessageDialog(null, "No se encontro ningúna tarjeta con ese pin.",
                         null, JOptionPane.WARNING_MESSAGE);
        
        }
        
        jtxt_pin.setText("");
        jtxt_saldo.setText(saldo);
        
    }

    
    /**
     * Responsable: Annie Paola Muñoz
     * @param jtxt_cedula
     * @param jtxt_descrip
     * @param jtxt_motivo
     * @param combo_tipo
     * @param combo_estacion
     * @param cedula_pasajero
     * @param descripcion
     * @param motivo
     * @param tipo
     * @param id_empleado
     * @param id_estacion
     * Método que ingresa una solicitud realizada por un pasajero del sistema, con la información
     * de la solicitud y del usuario.
     */
    public void generarSolicitud(JTextField jtxt_cedula, JTextArea jtxt_descrip, JTextField jtxt_motivo,JComboBox combo_tipo, JComboBox combo_estacion, String cedula_pasajero,
                            String descripcion, String motivo, String tipo, String id_empleado, String id_estacion) {
        
        jtxt_cedula.setText("");
        jtxt_descrip.setText("");
        jtxt_motivo.setText("");
        combo_estacion.setSelectedIndex(0);
        combo_tipo.setSelectedIndex(0);
                      
                
        int cantidad = 0;
         try {
             
             ResultSet result = auxiliar.consultarPasajero(cedula_pasajero);
             while(result.next()){
             
                 cantidad = Integer.parseInt(result.getString("cantidad"));
             }
             
             if(cantidad > 0){
             String director = "";
             resultado = auxiliar.consultarDirector(id_estacion);
             
             while(resultado.next()){
             
             
             director= resultado.getString("id_empleado");
             
         }
             
             if(auxiliar.generarSolicitud(cedula_pasajero, descripcion, motivo, tipo, id_empleado, id_estacion, fecha) == 0){
             
                 JOptionPane.showMessageDialog(null, "No se pudo registrar la solicitud",
                         null, JOptionPane.WARNING_MESSAGE);
                 
             }else{
                 
                 JOptionPane.showMessageDialog(null, "Se registro la solicitud de manera exitosa",
                         null, JOptionPane.WARNING_MESSAGE);
                         
             }                         
            }else{
             
                 JOptionPane.showMessageDialog(null, "Pasajero inexistente",
                         null, JOptionPane.WARNING_MESSAGE);
             
             
             } 
             
         } catch (SQLException ex) {
             Logger.getLogger(ControladorAuxiliar.class.getName()).log(Level.SEVERE, null, ex);
         }
        
        
    }

/**
     * Responsable: Annie Paola Muñoz
     * @param combo_estacion
     * Método que ingresa el nombre de las estaciones que hay registradas en el sistema
     * en el combo ingresado como parametro.
     */
    public void cargarEstaciones(JComboBox combo_estacion) {

        ResultSet resultado = auxiliar.consultarEstaciones();
        try {
            while(resultado.next()){
                
                combo_estacion.addItem(resultado.getString("nombre_estacion").toString());
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControladorAuxiliar.class.getName()).log(Level.SEVERE, null, ex);
        }
       
                                                                    
                    
        
    }

    /**
     * Responsable: Annie Paola Muñoz
     * @param estacion
     * @return ResultSet
     * Método que retorna el id de una estacion con el nombre ingresado como parametro.
     */
    public String consultarEstacion(String estacion) {

        ResultSet resultado = auxiliar.consultarEstacion(estacion);
        String id = "";
        try {
            while(resultado.next()){
                
                id = resultado.getString("id_estacion").toString();
                
            }   } catch (SQLException ex) {
            Logger.getLogger(ControladorAuxiliar.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return id;
        
    }

    /**
     * Responsable: Annie Paola Muñoz
     * @param pin
     * Método que bloquea una tarjeta con un pin en especifico.
     */
    public void bloquearTarjeta(String pin) {
        String resultado=auxiliar.bloquearTarjeta(pin);
        
        if(resultado.equals("yaEraInactiva"))
        {
            JOptionPane.showMessageDialog(null,"la tarjeta que intenta bloquear ya tiene le estado de inactiva");
        }else if (resultado.equals("fallo"))
        {
            JOptionPane.showMessageDialog(null,"no se encontro ninguna tarjeta con el pin ingresado");
        }else if (resultado.equals("exito"))
        {
            JOptionPane.showMessageDialog(null,"la tarjeta ha sido bloqueada");
        }
    }
    
    
    
    
    
    
    
    
    
}
