/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesoDatos;
import java.sql.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author desarrollo
 */
public class DaoDirectorEstacion {
    
    Fachada fachada = new Fachada();
    ResultSet respuesta;
    Statement instruccion;
    
    
    /**
     * Responsable: Esteban Aguirre Martinez
     *
     * @param restriccion
     * @param valor
     * @return ResultSet
     * Método que consulta informacion de las quejas segun una restriccion
     * y el valor de esta ingresada
     */
    public ResultSet consultarQuejas (String restriccion,String valor)
    {
        String orden = "SELECT n_tiquete,id_pasajero,tipo_queja,motivo_solicitud,fecha_solicitud,estado_solicitud FROM solicitudes WHERE "+restriccion+" = '"+valor+"';";
        
        //resultado de la consulta
        try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqle)
            {
                JOptionPane.showMessageDialog(null,"Error al consultar los datos de la tabla solicitudes: " + sqle,
                       "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
            {
                JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                        "Error",JOptionPane.ERROR_MESSAGE);
            }
            return respuesta;
                    
            }
    
    
    /**
     * Responsable: Esteban Aguirre Martinez
     *
     * @param ntiquete
     * @param solucion
     * @return boolean
     * Método que inserta una medida de solucion segun el numero del tiquete
     */
    public boolean resolverQuejas (String ntiquete,String solucion)
        {
        boolean respuesta=false;
        String orden = "INSERT INTO medidas_solucion VALUES ("+ntiquete+",'"+solucion+"');";
        
        //resultado de la consulta
        try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = !instruccion.execute(orden);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqle)
            {
                JOptionPane.showMessageDialog(null,"Error al ingresar los datos en la tabla medidas_solucion: " + sqle,
                       "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
            {
                JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                        "Error",JOptionPane.ERROR_MESSAGE);
            }
             
        return respuesta;
        }
    
    
    /**
     * Responsable: Esteban Aguirre Martinez
     *
     * @param ntiquete
     * @return ResultSet
     * Método que consulta informacion de un tiquete segun su numero
     */
    public ResultSet existeTiquete(String ntiquete)
    {
        String orden = "SELECT tipo_queja,motivo_solicitud,fecha_solicitud,estado_solicitud FROM solicitudes WHERE n_tiquete ='"+ntiquete+"';";
        //resultado de la consulta
        try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqle)
            {
                JOptionPane.showMessageDialog(null,"Error al consultar los datos de la tabla solicitudes: " + sqle,
                       "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
            {
                JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                        "Error",JOptionPane.ERROR_MESSAGE);
            }
        
        return respuesta;
    }
    
    /**
     * Responsable: Esteban Aguirre Martinez
     *
     * @param estado
     * @param ntiquete
     * @return String
     * Método que actualiza el estado de un tiquet segun su numero
     */
    public String peticionModificarEstadoReclamo(String estado,String ntiquete)
    {
        int modificadas=0;
        
        String orden = "UPDATE solicitudes SET estado_solicitud ='"+estado+"' WHERE n_tiquete ='"+ntiquete+"';";
        //resultado de la consulta
        try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            modificadas = instruccion.executeUpdate(orden);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqle)
            {
                JOptionPane.showMessageDialog(null,"Error al consultar los datos de la tabla solicitudes: " + sqle,
                       "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
            {
                JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                        "Error",JOptionPane.ERROR_MESSAGE);
            }
        
        if(modificadas==1)
        {
            return "correcto";
        }else
        {
            return "incorrecto";
        }
    }
    
}
