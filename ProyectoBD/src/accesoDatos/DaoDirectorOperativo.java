/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesoDatos;

import java.io.FileInputStream;
import java.sql.*;
import javax.swing.JOptionPane;


/**
 *
 * @author sara
 */
public class DaoDirectorOperativo {

    Fachada fachada;
    ResultSet respuesta;
    Statement instruccion;

    public DaoDirectorOperativo() 
    {
        fachada = new Fachada();
    }

    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @param placa
     * @param tipo
     * @param estado
     * @return boolean Método que realiza la peticion a la base de datos para
     * ingresar un nuevo bus Retorna un booleano que representa el exito de la
     * operacion
     */
    public boolean registrarBus(String placa, String tipo, String estado, String idRuta) {

        boolean resultado = false;

        String orden = "INSERT INTO buses VALUES ('" + placa + "','" + tipo + "','" + estado + "','"+idRuta+"');";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.execute(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return resultado;

    }

    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param nombre
     * @return ResultSet
     * Método que verifica si una ruta existe
     */
     public ResultSet peticionRutaExiste(String nombre)
    {
        String consultaSQL = "select count(*) from rutas where nombre_ruta = '"+nombre+"';";
        System.out.println(consultaSQL);
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
     /**
      * Responsable: Sara Catalina Muñoz H.
      * @param placa
      * @return ResultSet
      * Método que verifica si un bus ya se ha ingresado
      */
     public ResultSet peticionBusExiste(String placa)
    {
        String consultaSQL = "select count(*) from buses where placa = '"+placa+"';";
        System.out.println(consultaSQL);
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
     
    
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @param placa
     * @param tipo
     * @param estado
     * @return ResultSet Método que consulta la informacion respectiva de un bus
     * en especifico
     */
    public ResultSet consultarBus(String placa) {

        String orden = "select placa, tipo_bus, estado_bus, nombre_ruta from buses natural join rutas where placa ='" + placa + "';";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;

    }

    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @param placa
     * @param estado
     * @return boolean Método que realiza la peticion a la base de datos para
     * actualizar el estado de un bus Se retorna un booleano representando el
     * exito de la operacion
     */
    public boolean modificarBus(String placa, String estado, String ruta) {

        boolean resultado = false;

        String orden = "update buses set estado_bus = '" + estado + "', id_ruta = '"+ruta+"' where placa = '" + placa + "';";
        System.out.println(orden);
        
        
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.execute(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return resultado;

    }

    /**
     * Responsable: Sara Catalina Muñoz H.
     *
     * @param placa
     * @return boolean Método que realiza la peticion de eliminar un registro de
     * la base de datos de un bus en especifico. Retorna un boolean represntando
     * el éxito de la operacion.
     */
    public boolean eliminarBus(String placa) {

        boolean resultado = false;

        String orden = " delete from buses where placa = '" + placa + "';";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.execute(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return resultado;

    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param placa
     * @return boolean
     * Método que elimina los registros de la tabla turnos de un bus
     */
    public boolean eliminarTurno(String placa) {

        boolean resultado = false;

        String orden = " delete from turnos where placa_bus = '" + placa + "';";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.execute(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return resultado;

    }
    
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @return ResultSet
     * Método que consulta el nombre de las estaciones
     */
    public ResultSet consultarEstaciones() {

        String orden = "select nombre_estacion from estaciones";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;

    }
    
    /**
     * Responsable: Sara Catalina Muñoz H
     * @param placa
     * @return ResultSet
     * Método que consulta la ruta asociada a un bus
     */
     public ResultSet consultarRutadeBus(String placa) {

        String orden = "select nombre_ruta from rutas NATURAL JOIN buses WHERE placa = '"+placa+"';";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;

    }
    
    
 
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param nombre
     * @param recorrido
     * @param estado
     * @param fis
     * @param longitudBytes
     * @return boolean
     */
    public boolean agregarRuta(String nombre, String recorrido, String estado, FileInputStream fis, int longitudBytes) 
    {

        boolean resultado = false;

        String orden = " INSERT INTO rutas (nombre_ruta, descripcion_recorrido, estado_ruta, imagen)"
                + " VALUES (?, ?, ?, ?);";
        Connection conexion = fachada.conectar();
        try {
            PreparedStatement ps=conexion.prepareStatement(orden);
            ps.setString(1,nombre);
            ps.setString(2,recorrido);
            ps.setString(3,estado);
            ps.setBinaryStream(4,fis,longitudBytes);
            resultado = ps.execute();
            ps.close();
            
            
            System.out.println("parce que funciona");
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        System.out.println("! " + resultado);
        return resultado;

    }
    
    /**
     * Responsable: Sara Catalina Muñoz
     * @param nombre
     * @param descripcion
     * @param estado
     * @return boolean
     * Ingreso de una ruta sin una imagen asociada
     */
    public boolean agregarRutaSinImagen(String nombre, String descripcion, String estado) {

        boolean resultado = false;

        String orden = "insert into rutas (nombre_ruta, descripcion_recorrido, estado_ruta) values ('"+nombre
                +"', '"+descripcion+"' , '"+estado+"');";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.execute(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return resultado;

    }
    

    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param estacion
     * @param ruta
     * @return 
     */
    public boolean agregarRutasPasan(String estacion, String ruta) {

        boolean resultado = false;

        String orden = " insert into rutas_pasan (id_estacion, id_ruta) values('"+estacion+"', '"+ruta+"');";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.execute(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return resultado;

    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param nombre
     * @return ResultSet
     */
    public ResultSet consultarIdRuta(String nombre){
        String orden = "select id_ruta from rutas where nombre_ruta ='"+nombre+"';";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;
    }

    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param nombre
     * @return ResultSet
     */
    public ResultSet consultarIdEstacion(String nombre){
        String orden = "select id_estacion from estaciones where nombre_estacion ='"+nombre+"';";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @return ResultSet
     * Método que consulta el nombre de las rutas
     */
    public ResultSet consultarRutas() {

        String orden = "select nombre_ruta from rutas";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;

    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param horario
     * @param fecha
     * @return ResultSet
     * Método que hace la consulta de los conductores que se encuntran disponibles en una fecha
     * y turno específico
     */
     public ResultSet consultarConductoresDisponibles(String horario, String fecha) {

        String orden = "select id_empleado FROM empleados WHERE cargo_empleado ='conductor'" +
                       " EXCEPT " +
                       "select id_empleado FROM turnos WHERE fecha = '"+fecha+"' AND horario = '"+horario+"';";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;

    }
     
     /**
      * Responsable: Sara Catalina Muñoz Hueras
      * @param horario
      * @param fecha
      * @return ResultSet
      * Método que consulta buses disponibles segun un horario y fecha
      */
    public ResultSet consultarBusesDisponibles(String horario, String fecha) {

       String orden = " select placa from buses EXCEPT "+
               "select placa_bus from turnos where fecha ='"+fecha+"' and horario='"+horario+"';";
       
           System.out.println(orden);

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;

    }
    
     
     
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param id
     * @return 
     */
     public ResultSet consultarNombreEmpleado(String id) {

        String orden = "select nombre_empleado, apellido_empleado FROM empleados WHERE id_empleado = '"+id+"';";


        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;

    }
    
     
     /**
      * Responsable: Sara Catalina Muñoz H.
      * @param empleado
      * @param ruta
      * @param placa
      * @param fecha
      * @param horario
      * @return 
      */
      public boolean registrarTurno(String empleado, String placa, String fecha, String horario) {
          System.out.println("pase por aqui");
        boolean resultado = false;

        String orden = " insert into turnos values('"+empleado+"','"+placa+"','"+fecha+"','"+horario+"');";
          System.out.println(orden);
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.execute(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return resultado;

    }
    
      
      
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param placa
     * @return ResultSet
     * Método que consulta el id de una ruta segun la placa del bus que le corresponde
     */
      public ResultSet consultarIDRuta(String placa) {

       String orden = " select id_ruta from buses where placa ='"+placa+"';";
       
          System.out.println(orden);

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;

    }
      
      
      /**
       * Responsable: Sara Catalina Muñoz H.
       * @param id
       * @return ResultSet
       * Método que consulta el nombre de  una ruta segun su id
       */
       public ResultSet consultarNombreRuta(String id) {

       String orden = " select nombre_ruta from rutas where id_ruta ='"+id+"';";
       
           System.out.println(orden);

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;

    }
    
   
      
    /**Responsable: Sara Catalina Muñoz H.
     * @param id
     * @param fecha
     * @return ResultSet
     * Método que consulta relaliza la peticion a la base de datos de consultar el turno de un conductor
     */
    public ResultSet consultarTuno(String id, String fecha) {

        String orden = " select placa_bus, fecha, horario from turnos where id_empleado ='"+id+"' and"
                + " fecha = '"+fecha+"';";


        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;

    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @return ResultSet
     * Método que consulta las placas de los buses
     */
    public ResultSet consultarbuses() {

        String orden = " select placa from buses;";


        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;

    }

    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que permite ingresar la informacion de una estacion a la base de datos
     * de una estacion a la base de datos
     * @param nomb_Estacion 
     * @param ubicacion 
     * @param id_empleado 
     * @return int
     */
    public int peticionIngresarEstacion(String nomb_Estacion, String ubicacion,String id_empleado)
    {
        String consultaSQL = "INSERT INTO estaciones (nombre_estacion, ubicacion_estacion,id_empleado) "
                + "VALUES ('" + nomb_Estacion + "','" + ubicacion + "','" + id_empleado + "');";
        Connection conexion = fachada.conectar();
        int exito = 0;
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            exito = instruccion.executeUpdate(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return exito;
    }
    
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna toda la informacion de la tabla correspondiente a 
     * las rutas del sistema
     * @return ResultSet
     */
    public ResultSet peticionConsultarRutas()
    {
        String consultaSQL = "SELECT * FROM rutas;";
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que rertona aquellos empleados con el cargo de director de estacion
     * que no tiene una estacion asignada
     * @return ResultSet
     */
    public ResultSet peticionEmpleadosDESinEstacion()
    {
        String consultaSQL = "SELECT id_empleado,nombre_empleado FROM empleados "
                + "WHERE cargo_empleado = 'director estacion' AND estado_empleado = 'activo' " 
                + "EXCEPT SELECT id_empleado,nombre_empleado FROM empleados NATURAL JOIN estaciones;";
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta de empleados \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que le asigna a una ruta una imagen
     * @param id_ruta
     * @param fis
     * @param longitudBytes 
     * @return int
     */
    public int peticionAsignarImagenRuta(String id_ruta,FileInputStream fis, int longitudBytes)
    {
        String consultaSQL = "UPDATE rutas SET imagen = ? "
                + "WHERE id_ruta = '" + id_ruta + "';";
        Connection conexion = fachada.conectar();
        int exitoso = 0;
        try 
        {
            PreparedStatement ps = conexion.prepareStatement(consultaSQL);
            ps.setBinaryStream(1,fis,longitudBytes);
            exitoso = ps.executeUpdate();
            ps.close();
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) 
        {
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return exitoso;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que cambia la informacion de una ruta especifica en la base de datos
     * @param codigoRuta
     * @param nombreRuta
     * @param estado
     * @param descripcion
     * @return int
     */
    public int peticionModificarRuta(String codigoRuta,String nombreRuta,String estado,String descripcion)
    {
        String consultaSQL = "UPDATE rutas SET nombre_ruta = '" + nombreRuta + "',"
                + "estado_ruta = '" + estado + "',descripcion_recorrido = '" + descripcion + "' "
                + "WHERE id_ruta = '" + codigoRuta + "';";
        Connection conexion = fachada.conectar();
        int exitoso = 0;
        try 
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            exitoso = instruccion.executeUpdate(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return exitoso;
    }
    
}
