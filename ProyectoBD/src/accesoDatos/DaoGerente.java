/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesoDatos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author invitado
 */
public class DaoGerente {
     Fachada fachada;
    ResultSet respuesta;
    Statement instruccion;
    
    public DaoGerente(){
         fachada = new Fachada();
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @return Resultset
     * Método que consulta la informacion de los pasajeros
     */
     public ResultSet consultarPasajeros(){
      String consulta = "SELECT id_pasajero, nombre_pasajero, apellido_pasajero, telefono_pasajero, pin_tarjeta FROM pasajeros";
         
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    
    }
     
     
     /**
      * Responsable: Sara Catalina Muñoz H.
      * @return ResultSet
      * Método que consulta los años en los que se ha realizado una venta
      */
     public ResultSet consultarAnhosVentas(){
      String consulta = "select distinct EXTRACT(YEAR FROM fecha) FROM ventas;";
         
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    
    }
    
     
    /**
     * Responsable: Sara Catelina Muñoz H.
     * @param mes
     * @param anho
     * @return Resultset
     * Método que consulta la suma de las ventas realizadas en cada sede por mes
     */ 
     public ResultSet consultarVentasTarjetas(String mes, String anho){
      String consulta = "select ventas.id_estacion, nombre_estacion, sum(costo) FROM ventas INNER JOIN estaciones ON (ventas.id_estacion = estaciones.id_estacion)\n" +
    " GROUP BY ventas.id_estacion, nombre_estacion, fecha  HAVING EXTRACT(YEAR from fecha) = '"+anho+"' AND EXTRACT(MONTH from fecha) = '"+mes+"';";
         
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    
    }
     
     
     
     /**
     * Responsable: Esteban Aguirre Martinez.
     * @param cedula
     * @param nombre
     * @param apellido
     * @param cargo
     * @param salario
     * @param estado
     * @param empleadoControla
     * @param usuario
     * @param contraseña
     * @return String
     * Método que agrega un usuario (empleado) al sistema y retorna si la operacion fu eefectiva o no
     */
    public String peticionAgregarUsuario(String cedula,String nombre,String apellido,String cargo,String salario,String estado,String empleadoControla,String usuario,String contraseña)
    {
        boolean resultado = false;
        boolean existeUsuario = existeUsuario(cedula);
        
        if(!existeUsuario)
        {
            String orden = "INSERT INTO empleados VALUES ('" + cedula + "','" + nombre + "','" + apellido + "','" + cargo + "'," + salario + ",'" + estado + "','"+ empleadoControla +"','" + usuario + "','" + contraseña +"');";
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado=!instruccion.execute(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Se ha presentado un error en la insercion del nuevo usuario",
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        
        //si fue efectiva la operacion retorna la cadena correcto, en caso contrario retorna fallo
        if(resultado)
        {
            return "correcto";
        }else
            {
                return "fallo";
            }
        }else
        {
            return "fallo";
        }
        
  
    }
    
     /**
     * Responsable: Esteban Aguirre Martinez.
     * @return boolean
     * Método auxiliar qu everifica si existe o no un usuario con la cedula ingresada
     */
    public boolean existeUsuario(String cedula)
    {
        boolean resultado = false;
        int cantidad;
        String orden = "SELECT * FROM empleados WHERE id_empleado= '" + cedula + "';";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT 
            respuesta = instruccion.executeQuery(orden); 
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        //si fue efectiva la operacion retorna la cadena correcto, en caso contrario retorna fallo
        if(respuesta.next())
        {
            return true;
        }else
            {
                return false;
            }
        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "El usuario ya existe",
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        
        return false;
    }
    
    /**
     * Responsable: Esteban Aguirre Martinez.
     * @return ResultSet
     * Método que consulta la informacion de un usuario segun su cargo
     */
    public ResultSet consultarUsuariosCargo(String cargo)
    {
        String orden="";
        
        //si se consulta un cargo especifico se hace una consulta especifica, si se quieren
        //informacion de todos los empleados se realiza otra
        if(!cargo.equals("Todos"))
        {
            orden = "SELECT id_empleado,nombre_empleado,apellido_empleado,estado_empleado FROM empleados WHERE cargo_empleado='"+cargo+"';";
        }else
        {
            orden = "SELECT id_empleado,nombre_empleado,apellido_empleado,cargo_empleado,estado_empleado FROM empleados;";
        }
        

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
            
        //se retorna el ResultSet con los resultados de la consulta
        return respuesta;
    }
    
    /**
     * Responsable: Esteban Aguirre Martinez.
     * @return String
     * Método que conulsta la informacion de las rutas a las que un pasajero ingresa segun su id
     */
    public ResultSet consultarRutasFrecuentes(String id_pasajero)
    { 
        String orden = "SELECT id_pasajero,ingresos.id_ruta,count(*) cantidad,nombre_ruta,descripcion_recorrido FROM ingresos INNER JOIN rutas ON (rutas.id_ruta=ingresos.id_ruta) GROUP BY ingresos.id_ruta,id_pasajero,nombre_ruta,descripcion_recorrido HAVING id_pasajero='"+id_pasajero+"';";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna todas las rutas del sistema
     * @return ResultSet 
     */
    public ResultSet peticionRutasReporte()
    {
        String consulta = "SELECT id_ruta,nombre_ruta,descripcion_recorrido,estado_ruta FROM rutas";
         
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) 
        {
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    /**
     * Responsable: Annie Paola Muñoz Ll.
     * @return Resultset
     * Método que consulta el id y nombre de los conductores y la placa de los buses a los cuales han
     * sido asignados.
     */ 
       public ResultSet consultarBusesConductores() {
         String consulta = "SELECT placa, id_empleado, nombre_empleado, apellido_empleado FROM empleados NATURAL JOIN turnos "
                 + "INNER JOIN buses ON (placa = placa_bus)";
         
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
        
    }
       
    /**
     * Responsable: Annie Paola Muñoz Ll.
     * @param estacion
     * @return Resultset
     * Método que consulta el id de la estación según su id
     */ 
    public ResultSet consultarEstacion(String estacion) {
    String consulta = "SELECT id_estacion FROM estaciones  WHERE nombre_estacion = '"+estacion+"';";
         
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Por favor, verificar el nombre de la estación.",
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
        
    }
    
      /**
     * Responsable: Annie Paola Muñoz Ll.
     * @return Resultset
     * Método que consulta los años en los que se han hecho solicitudes
     */ 
    public ResultSet consultarAnhosSolicitudes() {
          String consulta = "SELECT DISTINCT EXTRACT(YEAR FROM fecha_solicitud) as años FROM solicitudes;";
         
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }    
        return respuesta;    
    }

     /**
     * Resposnable: Annie Paola Muñoz Ll.
     * @param mes
     * @param anio
     * @param estacion
     * @return ResultSet
     * Método que consulta la informacion de las solicitudes (id pasajero, descripción, estación) y quién las soluciono, de una
     * estación y fecha en especifico.
     */
    public ResultSet consultarSolcitudes(int mes, String anio, String estacion) 
    {
        String consulta = "  SELECT DISTINCT nombre_pasajero, apellido_pasajero, empleados.nombre_empleado as nombre_usuario, empleados.apellido_empleado,"
                          + " nombre_estacion, descripcion_solicitud as descripcion, director, director_apellido FROM empleados INNER JOIN solicitudes ON (id_empleado_atiende"
                          + " = id_empleado) INNER JOIN estaciones ON (estaciones.id_estacion = solicitudes.id_estacion) INNER JOIN (SELECT DISTINCT "
                          + "empleados.nombre_empleado as director, empleados.apellido_empleado as director_apellido, id_estacion  FROM estaciones NATURAL JOIN"
                          + " (SELECT id_estacion FROM solicitudes) as sub NATURAL JOIN empleados) as directores ON (directores.id_estacion = solicitudes.id_estacion)"
                          + " NATURAL JOIN pasajeros WHERE nombre_estacion = '"+estacion+"' AND EXTRACT(year FROM fecha_solicitud) = '"+anio+"' AND EXTRACT(month FROM fecha_solicitud) = '"+mes+"' ;";
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }    
        return respuesta; 
    }
    
    /**
     * Resposnable: Annie Paola Muñoz Ll.
     * @return ResultSet
     * Método que consulta el nombre de las estaciones que hay en el sistema.
     */
    public ResultSet consultarEstaciones() {
       String consulta = "SELECT nombre_estacion FROM estaciones;";
         
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    }
    
    /**
     * Resposnable: Esteban Aguirre Martinez
     * @return ResultSet
     * Método que consulta el id del pasajero, de la ruta y la fecha en que se realizo un ingreso.
     */
    public ResultSet consultarPasajerosMovilizados(int mes,String anio)
    {
        String consulta = " SELECT id_pasajero,id_ruta,fecha FROM ingresos WHERE EXTRACT (YEAR FROM fecha) = "+anio+" AND EXTRACT (MONTH FROM fecha) ="+mes+";";
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }    
        return respuesta;
    }
    
    /**
     * Resposnable: Esteban Aguirre Martinez
     * @return ResultSet
     * Método que consulta el tipo de queja y la cantidad de esta que se han realizado.
     */
    public ResultSet consultarSolcitudes()
    {
        String consulta = " SELECT tipo_queja,COUNT(*) cantidad_quejas FROM solicitudes GROUP BY tipo_queja ;";
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }    
        return respuesta;
    }
    
    /**
     * Resposnable: Esteban Aguirre Martinez
     * @return ResultSet
     * Método que consulta los años de los ingresos al sistema.
     */
    public ResultSet consultarAniosIngresos(){
      String consulta = "select distinct EXTRACT(YEAR FROM fecha) FROM ingresos;";
         
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    
    }
     
}
