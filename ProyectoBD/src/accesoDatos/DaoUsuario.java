/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesoDatos;

import java.net.URL;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author jhonfmr
 */
public class DaoUsuario 
{

    Fachada fachada;
    ResultSet respuesta;
    Statement instruccion;
    public DaoUsuario() 
    {
        fachada = new Fachada();
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna el pin de la tarjeta solicitada
     * @param pin_tarjeta
     * @return 
     */
    public ResultSet peticionPinTarjeta(int pin_tarjeta)
    {
        String consultaSQL = "SELECT pin FROM tarjetas "
                + "WHERE pin = " + pin_tarjeta + ";";
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna el id de un pasajero que tenga asociada una tarjeta
     * @param pin_tarjeta
     * @return ResultSet
     */
    public ResultSet consultaPasajero(int pin_tarjeta)
    {
        String consultaSQL = "SELECT id_pasajero FROM pasajeros "
                + "INNER JOIN tarjetas ON (pin = pin_tarjeta) WHERE pin = " + pin_tarjeta + ";";
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
        
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que registra el ingreso del usuario en la base de datos
     * @param id_pasajero
     * @param ruta
     * @param fecha
     * @param hora
     * @return int
     */
    public int peticionIngresarSistema(String id_pasajero,int ruta,String fecha, String hora)
    {
        String consultaSQL = "INSERT INTO ingresos VALUES('" + id_pasajero +"','" + ruta + "',"
                + "'" + fecha + "','" + hora +"');";
        Connection conexion = fachada.conectar();
        int exito = 0;
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            exito = instruccion.executeUpdate(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return exito;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna el saldo de un tarjeta 
     * @param pint_tarjeta
     * @return ResultSet
     */
    public ResultSet peticionRetornaSaldo(String pint_tarjeta)
    {
        String consultaSQL = "SELECT saldo FROM tarjetas WHERE pin = '" + pint_tarjeta + "';";
        Connection conexion = fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que descuenta el saldo de la tarjeta en la base de datos
     * @param pin_tarjeta
     * @return pin_tarjeta
     */
    public int peticionRestarSaldo(String pin_tarjeta)
    {
        String consultaSQL = "UPDATE tarjetas SET saldo = (SELECT saldo FROM tarjetas WHERE pin = " + pin_tarjeta + ") - 1800 "
                + "WHERE pin = " + pin_tarjeta + ";";
        Connection conexion = fachada.conectar();
        int exito = 0;
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            exito = instruccion.executeUpdate(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta de descuento de saldo. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return exito;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna todas las rutas del sistema
     * @return ResultSet
     */
    public ResultSet peticionConsultarRutas()
    {
        String consultaSQL = "SELECT id_ruta, nombre_ruta, descripcion_recorrido FROM rutas  WHERE estado_ruta = 'activa' "
                + "ORDER BY id_ruta;";
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna todas las rutas del sistema
     * @param nombreEstacion 
     * @return ResultSet
     */
    public ResultSet peticionBuscarRutas(String nombreEstacion)
    {
        String consultaSQL = "SELECT nombre_ruta,descripcion_recorrido FROM rutas_pasan "
                + "NATURAL JOIN estaciones "
                + "NATURAL JOIN rutas WHERE nombre_estacion = '" + nombreEstacion + "';";
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna el nombre de las estaciones del sistema
     * @return ResultSet
     */
    public ResultSet peticionConsultarNombreEstaciones()
    {
        String consultaSQL = "SELECT nombre_estacion FROM estaciones;";
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna de la base de datos, las rutas que pasan por una estacion de origen y una
     * estacion de destino
     * @param origen
     * @param destino
     * @return ResultSet
     */
    public ResultSet peticionRutasOrigenDestino(String origen, String destino)
    {
        String consultaSQL = "SELECT nombre_ruta,descripcion_recorrido FROM "
                + "(SELECT id_ruta FROM rutas_pasan NATURAL JOIN estaciones WHERE nombre_estacion = '" + origen + "' "
                + "INTERSECT SELECT id_ruta FROM rutas_pasan NATURAL JOIN estaciones WHERE nombre_estacion = '" + destino + "') REO_RED  "
                + "NATURAL JOIN rutas;";
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna una solicitud de queja o reclamo de la base de datos
     * segun un identificador 
     * @param numTiquete
     * @return ResultSet
     */
    public ResultSet peticionBuscarSolicitud(int numTiquete)
    {
        String consultaSQL = "SELECT n_tiquete,motivo_solicitud,fecha_solicitud,descripcion_Solicitud,"
                + "estado_solicitud,tipo_queja,id_pasajero,nombre_pasajero FROM solicitudes"
                + " NATURAL JOIN pasajeros WHERE n_tiquete = " + numTiquete + ";";
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retornas las medidas de solucion asociadas a un numero de tiquete
     * @param numTiquete
     * @return ResultSet
     */
    public ResultSet peticionBuscarMedidasSolucion(int numTiquete)
    {
        String consultaSQL = "SELECT medida_solucion FROM medidas_solucion "
                + "WHERE n_tiquete = " + numTiquete + ";";
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna la ruta de ubicacion de la imagen
     * @param nombreRuta
     * @return String
     */
    public URL peticionConsultarRutaImagen(String nombreRuta)
    {
        URL urlImagen = getClass().getResource("/imgRutas/" + nombreRuta + ".png");
        return  urlImagen;
    }       
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna la imagen asociada a una ruta de la base de datos
     * @param nombreRuta
     * @return ResultSet
     */
    public ResultSet peticionConsultarImagen(String nombreRuta)
    {
        String consultaSQL = "SELECT imagen FROM rutas "
                + "WHERE nombre_ruta = '" + nombreRuta + "';";
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }  
}
