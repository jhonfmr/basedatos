/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesoDatos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author sara
 */
public class DaoConductor {
    Fachada fachada;
    ResultSet respuesta;
    Statement instruccion;
  
    public DaoConductor(){
         fachada = new Fachada();
        
    }
    
    /**
     * Responsable: Sara Catalina Munoz
     * @param id
     * @param fecha
     * @return ResultSet
     * Método que consulta el turno de un conductor
     */
    public ResultSet consultarTurno(String id, String fecha) {

        String orden = "select placa_bus, fecha, horario from turnos where id_empleado='" + id + "' and fecha='"+fecha+"';";
        System.out.println(orden);
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;

    }
    
}
