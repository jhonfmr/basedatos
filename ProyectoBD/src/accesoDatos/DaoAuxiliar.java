/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesoDatos;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import javax.swing.JOptionPane;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author annie
 */
public class DaoAuxiliar {
    Fachada fachada;
    ResultSet respuesta;
    Statement instruccion;
    ResultSet resultado;

    public DaoAuxiliar() 
    {
        fachada = new Fachada();
    }
    
    
     /**
     * Responsable: Annie Paola Muñoz
     * @param saldo
     * @param estacion
     * Método que agrega el registro de una tarjeat en el sistema con un saldo y una estación
     * en específico
     */
     public int agregarTarjeta(String saldo, String estacion)
    {
        String orden = "INSERT INTO tarjetas (saldo, tipo, estado, id_estacion) VALUES ("+ saldo +", 'generica','activo','"+ estacion + "');";
        //Permite saber si la consulta SQL se hizo con exito
       int resultado = 0;
       Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.executeUpdate(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return resultado;
    }
     
     
     
     /**
     * Responsable: Annie Paola Muñoz
     * @param pin
     * @param recarga
     * @return int
     * Método que modifica el saldo de una tarjeta según el pin y el valor de recarga
     * ingresado como parámetro.
     */
     public int recargarTarjeta(String pin, int recarga){
        int resultado = 0;
    
    String consulta = "UPDATE tarjetas SET saldo ="+recarga+" WHERE pin = "+pin+";";
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.executeUpdate(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                    
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Por favor, verificar el pin de la tarjeta.",
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return resultado;   
     }
     
     
     
     /**
     * Responsable: Annie Paola Muñoz
     * @param pin
     * @return ResultSet
     * Método que consulta el saldo de una tarjeta con un pin en especifico.
     */
     public ResultSet consultarSaldo(String pin){
      String consulta = "SELECT saldo FROM tarjetas WHERE pin ="+pin+"";
         
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Por favor, verificar el pin de la tarjeta.",
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    
    }
     
      /**
     * Responsable: Annie Paola Muñoz
     * Método que consulta el pin de la ultima tarjeta registrada en el sistema.
     */
     public ResultSet consultarPin(){
     String consulta = "SELECT MAX(pin) pin FROM tarjetas;";
         
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
     
     }
          
     /**
     * Responsable: Annie Paola Muñoz
     * @param pin
     * @param id_empleado
     * @param estacion
     * @param costo
     * @param fecha
     * @return int
     * Método que registra una venta en el sistema con los datos especificados como parámetros
     * y retorna un número que confirma el registro.
     */
     public int venderTarjeta(String pin,String id_empleado, String estacion, String costo,String fecha){
     
     String orden = "INSERT INTO ventas (pin_tarjeta, id_empleado, id_estacion, fecha, costo) VALUES ("+ pin +",'"+id_empleado+"','"+ estacion +"', '"+fecha+"', "+costo+");";
        //Permite saber si la consulta SQL se hizo con exito
       int resultado = 0;
       Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.executeUpdate(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return resultado;
     }
     
     /**
     * Responsable: Annie Paola Muñoz
     * @param nombre
     * @param apellido
     * @param telefono
     * @param pin
     * @param cedula
     * @return int
     * Método que registra un nuevo pasajero en el sistema con los valores ingresados
     * por parametro.
     */
     public int agregarPasajero(String nombre, String apellido, String telefono, String pin, String cedula) {
        
         String orden = "INSERT INTO pasajeros VALUES ('"+ cedula +"',"+pin+",'"+nombre+"', '"+apellido+"', "+telefono+");";
        //Permite saber si la consulta SQL se hizo con exito
       int resultado = 0;
       Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.executeUpdate(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Por favor, verificar el pin de la tarjeta.",
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return resultado;
    }
  

   /**
     * Responsable: Annie Paola Muñoz
     * @param nombre
     * @param apellido
     * @param telefono
     * @param pin
     * @param cedula
     * @return int
     * Método que modifica los datos de un registro de un pasajero del sistema con una cedula y un pin de tarjeta
     * en especifico.
     */  
   public int modificarDatosPasajero(String nombre, String apellido, String telefono, String pin, String cedula) {
        
        String orden = "UPDATE pasajeros SET id_pasajero = '"+cedula+"', nombre_pasajero = '"+nombre+"', apellido_pasajero = '"+apellido+"',"
                      + " telefono_pasajero = '"+telefono+"' WHERE pin_tarjeta = "+pin+";";
        //Permite saber si la consulta SQL se hizo con exito
       int resultado = 0;
       Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.executeUpdate(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Está cédula ya existe en el sistema." ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return resultado;
    }
   
   
   /**
     * Responsable: Annie Paola Muñoz
     * @param pin
     * @return ResultSet
     * Método que consulta la informacion de un pasajero con una tarjeta en especifico
     * del sistema.
     */
    public ResultSet consultarInfoPasajero(String pin) {
    String consulta = "SELECT id_pasajero, nombre_pasajero, apellido_pasajero, telefono_pasajero FROM pasajeros "
            + "WHERE pin_tarjeta = "+pin+";";
                               
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Por favor, verificar el pin de la tarjeta.",
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;       
    }
    

    /**
     * Responsable: Annie Paola Muñoz
     * @param estacion
     * @return ResultSet
     * Método que consulta la cédula del director de una estación en especifico.
     */
    public ResultSet consultarDirector(String estacion) {
        String consulta = "SELECT id_empleado FROM estaciones WHERE id_estacion = "+estacion+";";
           
                               
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Por favor, verificar el id de la estación.",
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }    
        return respuesta;      
    }
    
    

    /**
     * Responsable: Annie Paola Muñoz
     * @param cedula_pasajero
     * @param descripcion
     * @param motivo
     * @param tipo
     * @param id_empleado
     * @param id_estacion
     * @param fecha
     * @return ResultSet
     * Método que registra en el sistema una solicitud realizada por un pasajeron en espcifico,
     * ingresada por un auxiliar en especifico y en una estacion en especifico.
     */
    public int  generarSolicitud(String cedula_pasajero, String descripcion, String motivo, String tipo, String id_empleado, String id_estacion, String fecha) {
        
     String orden = "INSERT INTO solicitudes (motivo_solicitud, fecha_solicitud, descripcion_solicitud, estado_solicitud, tipo_queja, id_pasajero,"
             + " id_estacion, id_empleado_atiende) VALUES ('"+motivo+"','"+fecha+"','"+descripcion+"', 'pendiente', '"+tipo+"',"
             + " '"+cedula_pasajero+"' , "+id_estacion+" , '"+id_empleado+"');";
     
        //Permite saber si la consulta SQL se hizo con exito
       int resultado = 0;
       Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.executeUpdate(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return resultado;                
    }
    
    
    /**
     * Responsable: Annie Paola Muñoz
     * @param cedula
     * @return ResultSet
     * Método que consulta si un pasajero existe en el sistema con la cédula
     * ingresada como parametro.
     */
    public ResultSet consultarPasajero(String cedula) {
String consulta = "SELECT COUNT(*) cantidad FROM pasajeros WHERE id_pasajero = '"+cedula+"';";
                               
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Por favor, verificar la cédula del pasajero.",
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;   
                                
    }

    
    /**
     * Responsable: Annie Paola Muñoz
     * @param estacion
     * @param pin
     * @param recarga
     * @param fecha
     * @param hora
     * @return int
     * Método que registra una recarga dentro del sistema con los datos especificados
     * como parámetros y retorna un número como confirmación.
     */
    public int ingresarRecarga(String estacion, String pin, int recarga, String fecha, String hora) {
       String orden = "INSERT INTO recargas  VALUES ("+estacion+","+pin+","+recarga+", '"+fecha+"' ,'"+hora+"');";
     
        //Permite saber si la consulta SQL se hizo con exito
       int resultado = 0;
       Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.executeUpdate(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Por favor, verificar el pin de la tarjeta.",
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return resultado;  
    }
    
   /**
     * Responsable: Annie Paola Muñoz
     * @return ResultSet
     * Método que consulta el nombre de las sedes que tiene registradas el sistema.
     */
    public ResultSet consultarEstaciones() {
        String consulta = "SELECT nombre_estacion FROM estaciones;";
         
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
        
        
    }

    /**
     * Responsable: Annie Paola Muñoz
     * @param estacion
     * @return ResultSet
     * Método que consulta el id de una estacion con el nombre ingresado como parametro de las estaciones
     * que hay en el sistema.
     */
    public ResultSet consultarEstacion(String estacion) {
    String consulta = "SELECT id_estacion FROM estaciones  WHERE nombre_estacion = '"+estacion+"';";
         
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Por favor, verificar el nombre de la estación.",
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
        
    }

    /**
     * Responsable: Annie Paola Muñoz
     * @param pin
     * @return int
     * Método que modifica el estado de una tarjeta con el pin ingresado como parametro
     * a personal.
     */
    public int personalizarTarjeta(String pin) {
        int resultado = 0;
    
    String consulta = "UPDATE tarjetas SET tipo = 'personal' WHERE pin = "+pin+" AND tipo = 'generica';";
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.executeUpdate(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                    
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Por favor, verificar el pin de la tarjeta.\nTarjeta no existente dentro de las generales.",
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return resultado; 
    }

    	
    /**
     * Responsable: Annie Paola Muñoz
     * @param pin
     * @return String
     * Método que bloquea una tarjeta, es decir modifica el estado de una tarjeta con un pin
     * en especifico.
     */	
    public String bloquearTarjeta(String pin) {
        int resultado = 0;
        String estado="inactiva";
    try
    {
        ResultSet comprobar = verificarInactiva(pin);
        if(comprobar.next())
        {
            estado = comprobar.getString("estado");
        }
        else return "fallo";
    }catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Por favor, verificar el pin de la tarjeta.\nTarjeta no existente dentro de las generales.",
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    if(estado.equals("activa"))
    {
        String consulta = "UPDATE tarjetas SET estado = 'inactiva' WHERE pin = "+pin+";";
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.executeUpdate(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
    
            return "exito"; 
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Por favor, verificar el pin de la tarjeta.\nTarjeta no existente dentro de las generales.",
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    }else
    {
        return "yaEraInactiva";
    }
        return "fallo";

    }
    
    /**
     * Responsable: Annie Paola Muñoz
     * @param pin
     * @return int
     * Método que consulta el estado de una tarjeta con el pin ingresado como parametro.   
     */
    public ResultSet verificarInactiva(String pin){
 
        String consulta = "SELECT estado FROM tarjetas WHERE pin = "+pin+";";
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                    
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Por favor, verificar el pin de la tarjeta.\nTarjeta no existente dentro de las generales.",
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return resultado; 
    }
}
