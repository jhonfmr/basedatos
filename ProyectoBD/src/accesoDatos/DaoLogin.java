/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesoDatos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import accesoDatos.Fachada;
import java.sql.Statement;
/**
 *
 * @author sara
 */
public class DaoLogin {
        
      Fachada fachada;
    ResultSet respuesta;
    Statement instruccion;
    
    
    public DaoLogin(){
        fachada = new Fachada();
    }
        
    /**
     * Responsable: Jhon Frederick Mena R
     * @param user
     * @param password
     * @return ResultSet
     */
    public ResultSet peticionCargo(String user, String password)
    {
        String consultaSQL = "SELECT usuario,password,cargo_empleado,id_empleado FROM empleados "
                + "WHERE usuario = '" + user + "' AND password = '" + password +"';";
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param id
     * @return ResultSet
     * Método que consulta el nombre de un empleado segun su cedula
     */
    public ResultSet peticionNombre(String id)
    {
        String consultaSQL = "SELECT nombre_empleado, apellido_empleado FROM empleados "
                + "WHERE id_empleado='"+id+"';";
        Connection conexion =  fachada.conectar();
        try
        {
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(consultaSQL);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqlex)
        {
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la consulta. \n" + sqlex,
            "Error SQL",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    
}
