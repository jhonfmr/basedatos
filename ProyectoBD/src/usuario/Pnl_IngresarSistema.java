/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;


import java.awt.event.*;
import controlador.ControladorUsuario;
import java.util.GregorianCalendar;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
/**
 *
 * @author jhonfmr
 */
public class Pnl_IngresarSistema extends javax.swing.JPanel {

    Manejadora maneja;
    DefaultComboBoxModel<String> modelo;
    /**
     * Creates new form pnl_AgregarUsuario
     */
    public Pnl_IngresarSistema() 
    {
        initComponents();
        maneja = new Manejadora();
        modelo = new DefaultComboBoxModel<String>();
        modelo.addElement("seleccione...");
        jcb_rutasISU.setModel(modelo);
        
        /* Adicion de las escuchas KeyListener */
        txt_ingresarPinISU.addKeyListener(maneja);

        rellenarRutas();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbl_tituloISU = new javax.swing.JLabel();
        txt_ingresarPinISU = new javax.swing.JTextField();
        lbl_pinTarjetaISU = new javax.swing.JLabel();
        pnl_rutasDisponiblesISU = new javax.swing.JPanel();
        jcb_rutasISU = new javax.swing.JComboBox();
        lbl_inicioISU = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 36, 84));
        setLayout(null);

        lbl_tituloISU.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lbl_tituloISU.setForeground(java.awt.Color.white);
        lbl_tituloISU.setText("Ingresar al Sistema");
        add(lbl_tituloISU);
        lbl_tituloISU.setBounds(300, 210, 250, 40);
        add(txt_ingresarPinISU);
        txt_ingresarPinISU.setBounds(390, 320, 220, 27);

        lbl_pinTarjetaISU.setForeground(java.awt.Color.white);
        lbl_pinTarjetaISU.setText("Pin Tarjeta");
        add(lbl_pinTarjetaISU);
        lbl_pinTarjetaISU.setBounds(270, 330, 90, 17);

        pnl_rutasDisponiblesISU.setBackground(java.awt.Color.white);
        pnl_rutasDisponiblesISU.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Rutas disponibles", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Andale Mono", 1, 14), java.awt.Color.white)); // NOI18N
        pnl_rutasDisponiblesISU.setForeground(java.awt.Color.white);
        pnl_rutasDisponiblesISU.setOpaque(false);
        pnl_rutasDisponiblesISU.setLayout(null);

        pnl_rutasDisponiblesISU.add(jcb_rutasISU);
        jcb_rutasISU.setBounds(60, 30, 170, 27);

        add(pnl_rutasDisponiblesISU);
        pnl_rutasDisponiblesISU.setBounds(280, 380, 300, 90);

        lbl_inicioISU.setBackground(java.awt.Color.white);
        lbl_inicioISU.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        lbl_inicioISU.setForeground(java.awt.Color.white);
        lbl_inicioISU.setText("Bienvenido al sistema de trasnporte MIO");
        add(lbl_inicioISU);
        lbl_inicioISU.setBounds(80, 90, 703, 43);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox jcb_rutasISU;
    private javax.swing.JLabel lbl_inicioISU;
    private javax.swing.JLabel lbl_pinTarjetaISU;
    private javax.swing.JLabel lbl_tituloISU;
    private javax.swing.JPanel pnl_rutasDisponiblesISU;
    private javax.swing.JTextField txt_ingresarPinISU;
    // End of variables declaration//GEN-END:variables

    /**
     * Responsable:Jhon Frederick Mena R
     * Metodo que rellena el combo que contendra las rutas del sistema
     */
    public void rellenarRutas()
    {
        ControladorUsuario controlador = new ControladorUsuario();
        controlador.rellenarRutas(modelo);
    }
    
    public class Manejadora implements KeyListener
    {
        ControladorUsuario controlador = new ControladorUsuario();

        @Override
        public void keyTyped(KeyEvent e) 
        {
            if(e.getSource() == txt_ingresarPinISU)
            {
                int caracter = e.getKeyChar();
                if(caracter == 10)
                {
                    String pin = txt_ingresarPinISU.getText();
                    if(!pin.isEmpty())
                    {
                        GregorianCalendar calendario = new GregorianCalendar();

                        //Se toma la fecha del sistema
                        int año = calendario.get(calendario.YEAR);
                        int mes = calendario.get(calendario.MONTH);
                        int dia = calendario.get(calendario.DAY_OF_MONTH);
                        String fecha = año + "-" + (mes + 1) + "-" + dia;

                        //Se toma la hora del sistema                   
                        int horas = calendario.get(calendario.HOUR);
                        int minutos = calendario.get(calendario.MINUTE);
                        int segundos = calendario.get(calendario.SECOND);
                        String hora = horas + ":" + minutos + ":" + segundos;

                        int pinTarjeta = Integer.parseInt(pin);
                        //Se toma el id de la ruta
                        int id_ruta = jcb_rutasISU.getSelectedIndex();
                        if (id_ruta != 0) 
                        {
                            //Determinara si la tarjeta esta o no en el sistema
                            if(controlador.verificarPin(pinTarjeta)) 
                            {
                                //Determinara si la tarjeta es personalizada 
                                String pasajero = controlador.retornarPasajero(pinTarjeta);
                                if (!pasajero.isEmpty()) 
                                {
                                    //Retornara el saldo actual de la tarjeta
                                    int saldo = controlador.retornarSaldo(pin);
                                    //Si el saldo no es negativo, se puede descontar el pasaje
                                    if (!(saldo < -5400)) 
                                    {
                                        //Se toma el nuevo saldo 
                                        String nuevo_saldo = controlador.descontarSaldo(pin);
                                        //Se ingresa el pasajero que tiene tarjeta personalizada
                                        if (controlador.ingresarSistema(pasajero, id_ruta, fecha, hora)) {
                                            txt_ingresarPinISU.setText("");
                                            jcb_rutasISU.setSelectedIndex(0);

                                            JOptionPane.showMessageDialog(null, "Saldo: " + nuevo_saldo,
                                                    "Advertencia", JOptionPane.WARNING_MESSAGE);
                                        } else 
                                        {
                                            JOptionPane.showMessageDialog(null, "Error al ingresar al sistema",
                                                    "Error", JOptionPane.ERROR_MESSAGE);
                                        }

                                    } else 
                                    {
                                        JOptionPane.showMessageDialog(null, "Saldo insuficiente",
                                                "Advertencia", JOptionPane.WARNING_MESSAGE);
                                    }
                                } //Para pasajeros con tarjeta general
                                else 
                                {
                                    //Retornara el saldo actual de la tarjeta
                                    int saldo = controlador.retornarSaldo(pin);
                                    //Si el saldo no es negativo, se puede descontar el pasaje
                                    if (!(saldo < 0)) 
                                    {
                                        //Se toma el nuevo saldo 
                                        String nuevo_saldo = controlador.descontarSaldo(pin);
                                        JOptionPane.showMessageDialog(null, "Saldo: " + nuevo_saldo,
                                                "Advertencia", JOptionPane.WARNING_MESSAGE);
                                    } else 
                                    {
                                        JOptionPane.showMessageDialog(null, "Saldo insuficiente",
                                                "Advertencia", JOptionPane.WARNING_MESSAGE);
                                    }

                                }
                            }else
                            {          
                                JOptionPane.showMessageDialog(null, "La tarjeta: " + pinTarjeta + " no esta registrada",
                                        "Advertencia", JOptionPane.WARNING_MESSAGE);
 
                            }                            
                        } else 
                        {
                            JOptionPane.showMessageDialog(null, "Debe seleccionar primero una ruta",
                                    "Advertencia", JOptionPane.WARNING_MESSAGE);
                        }
                    }else
                    {
                        JOptionPane.showMessageDialog(null, "No se ha ingresado el pin de la tarjeta",
                                    "Advertencia", JOptionPane.WARNING_MESSAGE);
                    }
                    
                }
                //Solo permitira ingresar caracteres numericos 
                else if (!(caracter >= 48 && caracter <= 57)) 
                {
                    e.setKeyChar((char) e.VK_CLEAR);
                }
            
            }
        }

        @Override
        public void keyPressed(KeyEvent e) 
        {
        }

        @Override
        public void keyReleased(KeyEvent e) 
        {
        }
               
    }
}
