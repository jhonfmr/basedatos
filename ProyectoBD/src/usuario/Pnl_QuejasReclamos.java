/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

import controlador.ControladorUsuario;
import java.awt.event.*;

/**
 *
 * @author jhonfmr
 */
public class Pnl_QuejasReclamos extends javax.swing.JPanel {

    Manejadora maneja;
    /**
     * Creates new form Pnl_QuejasReclamos
     */
    public Pnl_QuejasReclamos() {
        initComponents();
        area_informacionQRU.setEditable(false);
        maneja = new Manejadora();
        /* Adicion de las escuchas ActionListener */
        btn_buscarQRU.addActionListener(maneja);
         /* Adicion de las escuchas keyListener */
         txt_NoQuejaReclamoQRU.addKeyListener(maneja);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbl_inicioEDU = new javax.swing.JLabel();
        lbl_tituloEDU = new javax.swing.JLabel();
        pnl_busquedaQuejasQRU = new javax.swing.JPanel();
        pnll_NoQuejaReclamoQRU = new javax.swing.JPanel();
        btn_buscarQRU = new javax.swing.JButton();
        txt_NoQuejaReclamoQRU = new javax.swing.JTextField();
        lbl_NoQuejaReclamoQRU = new javax.swing.JLabel();
        pnl_centralBusqQRU = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        area_informacionQRU = new javax.swing.JTextArea();

        setBackground(new java.awt.Color(0, 36, 84));
        setPreferredSize(new java.awt.Dimension(1090, 540));
        setLayout(null);

        lbl_inicioEDU.setBackground(java.awt.Color.white);
        lbl_inicioEDU.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        lbl_inicioEDU.setForeground(java.awt.Color.white);
        lbl_inicioEDU.setText("Bienvenido al sistema de trasnporte MIO");
        add(lbl_inicioEDU);
        lbl_inicioEDU.setBounds(70, 50, 703, 43);

        lbl_tituloEDU.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lbl_tituloEDU.setForeground(java.awt.Color.white);
        lbl_tituloEDU.setText("Consultar Queja o Reclamo");
        add(lbl_tituloEDU);
        lbl_tituloEDU.setBounds(250, 130, 330, 28);

        pnl_busquedaQuejasQRU.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Busqueda del Reclamo/Queja", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14), java.awt.Color.white)); // NOI18N
        pnl_busquedaQuejasQRU.setOpaque(false);
        pnl_busquedaQuejasQRU.setLayout(new java.awt.BorderLayout());

        pnll_NoQuejaReclamoQRU.setOpaque(false);
        pnll_NoQuejaReclamoQRU.setPreferredSize(new java.awt.Dimension(50, 75));
        pnll_NoQuejaReclamoQRU.setLayout(null);

        btn_buscarQRU.setText("Buscar");
        pnll_NoQuejaReclamoQRU.add(btn_buscarQRU);
        btn_buscarQRU.setBounds(460, 20, 90, 29);
        pnll_NoQuejaReclamoQRU.add(txt_NoQuejaReclamoQRU);
        txt_NoQuejaReclamoQRU.setBounds(250, 20, 180, 27);

        lbl_NoQuejaReclamoQRU.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        lbl_NoQuejaReclamoQRU.setForeground(java.awt.Color.white);
        lbl_NoQuejaReclamoQRU.setText("No. Reclamo/Queja:");
        pnll_NoQuejaReclamoQRU.add(lbl_NoQuejaReclamoQRU);
        lbl_NoQuejaReclamoQRU.setBounds(70, 30, 160, 20);

        pnl_busquedaQuejasQRU.add(pnll_NoQuejaReclamoQRU, java.awt.BorderLayout.NORTH);

        pnl_centralBusqQRU.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnl_centralBusqQRU.setOpaque(false);
        pnl_centralBusqQRU.setLayout(new java.awt.GridLayout(1, 0));

        area_informacionQRU.setColumns(20);
        area_informacionQRU.setRows(5);
        area_informacionQRU.setMargin(new java.awt.Insets(20, 20, 20, 20));
        jScrollPane1.setViewportView(area_informacionQRU);

        pnl_centralBusqQRU.add(jScrollPane1);

        pnl_busquedaQuejasQRU.add(pnl_centralBusqQRU, java.awt.BorderLayout.CENTER);

        add(pnl_busquedaQuejasQRU);
        pnl_busquedaQuejasQRU.setBounds(70, 180, 700, 330);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea area_informacionQRU;
    private javax.swing.JButton btn_buscarQRU;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbl_NoQuejaReclamoQRU;
    private javax.swing.JLabel lbl_inicioEDU;
    private javax.swing.JLabel lbl_tituloEDU;
    private javax.swing.JPanel pnl_busquedaQuejasQRU;
    private javax.swing.JPanel pnl_centralBusqQRU;
    private javax.swing.JPanel pnll_NoQuejaReclamoQRU;
    private javax.swing.JTextField txt_NoQuejaReclamoQRU;
    // End of variables declaration//GEN-END:variables

    public class Manejadora implements ActionListener,KeyListener
    {
        ControladorUsuario controlador = new ControladorUsuario();
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            if(e.getSource() == btn_buscarQRU)
            {
                String num_Solicitud = txt_NoQuejaReclamoQRU.getText();
                int tiquete = Integer.parseInt(num_Solicitud);
                String info = controlador.buscarSolicitud(tiquete);
                area_informacionQRU.setText(info);
            }
        }

        @Override
        public void keyTyped(KeyEvent e) 
        {
            if(e.getSource() == txt_NoQuejaReclamoQRU)
            {
                //Toma el caracter digitado
                int caracter = (int) e.getKeyChar();
                //Solo permitira ingresar caracteres numericos
                if (!(caracter >= 48 && caracter <= 57)) 
                {
                    e.setKeyChar((char) e.VK_CLEAR);
                }
            }
            
        }

        @Override
        public void keyPressed(KeyEvent e) 
        {
        }

        @Override
        public void keyReleased(KeyEvent e) 
        {
        }
        
    }
}
