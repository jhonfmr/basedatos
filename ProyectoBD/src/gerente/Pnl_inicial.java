/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gerente;

/**
 *
 * @author sara
 */
public class Pnl_inicial extends javax.swing.JPanel {

    /**
     * Creates new form Pnl_inicial
     */
    static String nombre;
    public Pnl_inicial(String nombre_emp) {
        
        initComponents();
        nombre = nombre_emp;
        lbl_nombre.setText("Nombre: "+nombre);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lbl_nombre = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 36, 84));
        setMinimumSize(new java.awt.Dimension(670, 850));
        setPreferredSize(new java.awt.Dimension(850, 670));
        setLayout(null);

        jLabel1.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        jLabel1.setForeground(java.awt.Color.white);
        jLabel1.setText("Santiago de Cali");
        add(jLabel1);
        jLabel1.setBounds(330, 440, 120, 21);

        jLabel2.setFont(new java.awt.Font("Ubuntu", 1, 22)); // NOI18N
        jLabel2.setForeground(java.awt.Color.white);
        jLabel2.setText("Sistema de Transporte");
        add(jLabel2);
        jLabel2.setBounds(280, 130, 250, 30);

        jLabel3.setFont(new java.awt.Font("Ubuntu", 1, 22)); // NOI18N
        jLabel3.setForeground(java.awt.Color.white);
        jLabel3.setText("Masivo Integrado de Occidente");
        add(jLabel3);
        jLabel3.setBounds(230, 170, 340, 29);

        jLabel4.setFont(new java.awt.Font("Ubuntu", 0, 17)); // NOI18N
        jLabel4.setForeground(java.awt.Color.white);
        jLabel4.setText("Bienvenido");
        add(jLabel4);
        jLabel4.setBounds(350, 250, 90, 23);

        lbl_nombre.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        lbl_nombre.setForeground(java.awt.Color.white);
        lbl_nombre.setText("Nombre");
        add(lbl_nombre);
        lbl_nombre.setBounds(360, 310, 190, 21);

        jLabel6.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        jLabel6.setForeground(java.awt.Color.white);
        jLabel6.setText("Cargo: Gerente");
        add(jLabel6);
        jLabel6.setBounds(360, 350, 190, 21);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/usuario.png"))); // NOI18N
        add(jLabel5);
        jLabel5.setBounds(270, 300, 70, 70);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel lbl_nombre;
    // End of variables declaration//GEN-END:variables
}
