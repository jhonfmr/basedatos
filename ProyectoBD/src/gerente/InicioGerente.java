/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gerente;

import accesoDatos.Login;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author sara
 */

public class InicioGerente extends javax.swing.JFrame {
    static String cedula;
    static String nombre;
    
ManejadoraIG maneja = new ManejadoraIG();

Pnl_AgregarUsuario panelAgregarUser ;
Pnl_RutasFrecuentes panelPregFrec = new Pnl_RutasFrecuentes();
Pnl_ListarUsuarios panelListUser = new Pnl_ListarUsuarios();
Pnl_Reportes panelReport;
Pnl_inicial pnl_inicial;

    /**
     * Creates new form InicioGerente
     */
    public InicioGerente(String ced, String nom) {
        initComponents();
        cedula = ced;
        nombre= nom;
        panelAgregarUser = new Pnl_AgregarUsuario(cedula);
        panelPregFrec = new Pnl_RutasFrecuentes();
        panelListUser = new Pnl_ListarUsuarios();
        panelReport = new Pnl_Reportes(cedula,nombre);
        pnl_inicial = new Pnl_inicial(cedula);
        setLocationRelativeTo(null);
        maneja = new ManejadoraIG();
        
        btn_AgregarUsuario.addActionListener(maneja);
        btn_listarUsuarios.addActionListener(maneja);
        btn_reportes.addActionListener(maneja);
        btn_rutasFrecuentes.addActionListener(maneja);
        btn_cerrar.addActionListener(maneja);
        
        pnl_inicio.add(pnl_inicial,"1");
        pnl_inicio.add(panelAgregarUser,"2");
        pnl_inicio.add(panelPregFrec,"3");
        pnl_inicio.add(panelListUser,"4");
        pnl_inicio.add(panelReport,"5");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        pnl_inicio = new javax.swing.JPanel();
        pnl_izq = new javax.swing.JPanel();
        btn_AgregarUsuario = new javax.swing.JButton();
        btn_rutasFrecuentes = new javax.swing.JButton();
        btn_listarUsuarios = new javax.swing.JButton();
        btn_reportes = new javax.swing.JButton();
        lbl_mio = new javax.swing.JLabel();
        btn_cerrar = new javax.swing.JButton();

        jToolBar1.setRollover(true);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1090, 670));
        setResizable(false);

        pnl_inicio.setBackground(new java.awt.Color(0, 36, 84));
        pnl_inicio.setMinimumSize(new java.awt.Dimension(850, 670));
        pnl_inicio.setPreferredSize(new java.awt.Dimension(850, 670));
        pnl_inicio.setLayout(new java.awt.CardLayout());
        getContentPane().add(pnl_inicio, java.awt.BorderLayout.CENTER);

        pnl_izq.setBackground(java.awt.Color.white);
        pnl_izq.setPreferredSize(new java.awt.Dimension(240, 650));
        pnl_izq.setLayout(null);

        btn_AgregarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/AGREGARuSUARIO.png"))); // NOI18N
        btn_AgregarUsuario.setText("jButton1");
        btn_AgregarUsuario.setMaximumSize(new java.awt.Dimension(220, 80));
        btn_AgregarUsuario.setMinimumSize(new java.awt.Dimension(220, 80));
        btn_AgregarUsuario.setPreferredSize(new java.awt.Dimension(220, 80));
        btn_AgregarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AgregarUsuarioActionPerformed(evt);
            }
        });
        pnl_izq.add(btn_AgregarUsuario);
        btn_AgregarUsuario.setBounds(10, 140, 220, 80);

        btn_rutasFrecuentes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/RUTASFRECUENTES.png"))); // NOI18N
        btn_rutasFrecuentes.setText("jButton1");
        btn_rutasFrecuentes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_rutasFrecuentesActionPerformed(evt);
            }
        });
        pnl_izq.add(btn_rutasFrecuentes);
        btn_rutasFrecuentes.setBounds(10, 230, 220, 80);

        btn_listarUsuarios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/LISTARUSUARIOS.png"))); // NOI18N
        btn_listarUsuarios.setText("jButton1");
        btn_listarUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_listarUsuariosActionPerformed(evt);
            }
        });
        pnl_izq.add(btn_listarUsuarios);
        btn_listarUsuarios.setBounds(10, 320, 220, 80);

        btn_reportes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REPORTES.png"))); // NOI18N
        btn_reportes.setText("jButton1");
        btn_reportes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reportesActionPerformed(evt);
            }
        });
        pnl_izq.add(btn_reportes);
        btn_reportes.setBounds(10, 410, 220, 80);

        lbl_mio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/mio.png"))); // NOI18N
        pnl_izq.add(lbl_mio);
        lbl_mio.setBounds(0, 0, 220, 100);

        btn_cerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir.png"))); // NOI18N
        pnl_izq.add(btn_cerrar);
        btn_cerrar.setBounds(20, 100, 50, 30);

        getContentPane().add(pnl_izq, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_AgregarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AgregarUsuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_AgregarUsuarioActionPerformed

    private void btn_rutasFrecuentesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_rutasFrecuentesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_rutasFrecuentesActionPerformed

    private void btn_listarUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_listarUsuariosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_listarUsuariosActionPerformed

    private void btn_reportesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_reportesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_reportesActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InicioGerente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InicioGerente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InicioGerente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InicioGerente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InicioGerente(cedula, nombre).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_AgregarUsuario;
    private javax.swing.JButton btn_cerrar;
    private javax.swing.JButton btn_listarUsuarios;
    private javax.swing.JButton btn_reportes;
    private javax.swing.JButton btn_rutasFrecuentes;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel lbl_mio;
    private javax.swing.JPanel pnl_inicio;
    private javax.swing.JPanel pnl_izq;
    // End of variables declaration//GEN-END:variables

public class ManejadoraIG implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent ae) {
            if(ae.getSource() == btn_AgregarUsuario)
            {
                CardLayout distribuidor = (CardLayout) pnl_inicio.getLayout();
                distribuidor.show(pnl_inicio, "2");
              
            }else if(ae.getSource()== btn_rutasFrecuentes){
                CardLayout distribuidor = (CardLayout) pnl_inicio.getLayout();
                distribuidor.show(pnl_inicio, "3");
             
            }else if(ae.getSource()== btn_listarUsuarios){
                CardLayout distribuidor = (CardLayout) pnl_inicio.getLayout();
                distribuidor.show(pnl_inicio, "4");
              
            }else if(ae.getSource() == btn_reportes){
                CardLayout distribuidor = (CardLayout) pnl_inicio.getLayout();
                distribuidor.show(pnl_inicio, "5");
               
            }else if (ae.getSource()==btn_cerrar){
                int result = JOptionPane.showConfirmDialog(null, "¿Desea cerrar sesión?",
                        "Atención", JOptionPane.OK_CANCEL_OPTION);

                if (result == JOptionPane.OK_OPTION) {

                    Login interfaz = new Login();
                    interfaz.setVisible(true);
                    dispose();
            }
        }
    
}
}
}
