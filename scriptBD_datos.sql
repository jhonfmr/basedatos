--Annie Paola Muñoz - 1424440
--Jhon Frederick Mena - 1424886
--Sara Catalina Muñoz - 1422691
--Esteban Aguirre Martinez - 1427374

DROP TABLE IF EXISTS estaciones CASCADE;
DROP TABLE IF EXISTS tarjetas CASCADE;
DROP TABLE IF EXISTS empleados CASCADE;
DROP TABLE IF EXISTS rutas CASCADE;
DROP TABLE IF EXISTS buses CASCADE;
DROP TABLE IF EXISTS ventas CASCADE;
DROP TABLE IF EXISTS pasajeros CASCADE;
DROP TABLE IF EXISTS solicitudes CASCADE;
DROP TABLE IF EXISTS medidas_solucion CASCADE;
DROP TABLE IF EXISTS ingresos CASCADE;
DROP TABLE IF EXISTS rutas_pasan CASCADE;
DROP TABLE IF EXISTS turnos CASCADE;
DROP TABLE IF EXISTS recargas;


CREATE TABLE empleados
(
	id_empleado varchar(20) PRIMARY KEY,
	nombre_empleado varchar(50) NOT NULL,
	apellido_empleado varchar(50) NOT NULL,
	cargo_empleado varchar(20) NOT NULL,
	salario_empleado float NOT NULL,
	estado_empleado varchar(20) NOT NULL,
	id_empleado_controla varchar(20) NOT NULL,
	usuario varchar(20) NOT NULL UNIQUE,
	password varchar(20) NOT NULL
);

CREATE TABLE estaciones
(
	id_estacion serial PRIMARY KEY,
	nombre_estacion varchar(50) NOT NULL UNIQUE,
	ubicacion_estacion varchar(50) NOT NULL,
	id_empleado varchar(20) NOT NULL REFERENCES empleados (id_empleado)
		ON UPDATE CASCADE ON DELETE NO ACTION
);

CREATE TABLE tarjetas
(
	pin serial PRIMARY KEY,
	saldo integer NOT NULL,
	tipo varchar(10),
	estado varchar(20) NOT NULL,
	id_estacion serial NOT NULL REFERENCES estaciones (id_estacion)
		ON UPDATE CASCADE ON DELETE NO ACTION
);

CREATE TABLE rutas
(
	id_ruta serial PRIMARY KEY,
	nombre_ruta varchar(50) NOT NULL UNIQUE,
	descripcion_recorrido varchar (500) NOT NULL,
	estado_ruta varchar(30) NOT NULL,
	imagen bytea
);

CREATE TABLE buses
(
	placa varchar(10) PRIMARY KEY,
	tipo_bus varchar(20) NOT NULL,
	estado_bus varchar(10) NOT NULL,
	id_ruta serial NOT NULL REFERENCES rutas(id_ruta) 
);


CREATE TABLE ventas
(
	id_venta serial PRIMARY KEY,
	pin_tarjeta integer NOT NULL REFERENCES tarjetas (pin)
		ON UPDATE CASCADE ON DELETE NO ACTION,
	id_empleado varchar(20) NOT NULL REFERENCES empleados (id_empleado)
		ON UPDATE CASCADE ON DELETE NO ACTION,
	id_estacion integer NOT NULL REFERENCES estaciones (id_estacion)
		ON UPDATE CASCADE ON DELETE NO ACTION,
	fecha date NOT NULL,
	costo integer NOT NULL
);

CREATE TABLE pasajeros
(
	id_pasajero varchar(20) PRIMARY KEY,
	pin_tarjeta integer NOT NULL REFERENCES tarjetas (pin)
		ON UPDATE CASCADE ON DELETE NO ACTION,
	nombre_pasajero varchar(50) NOT NULL,
	apellido_pasajero varchar(50) NOT NULL,
	telefono_pasajero varchar(12) NOT NULL
);

CREATE TABLE solicitudes
(
	n_tiquete serial PRIMARY KEY,
	motivo_solicitud varchar(30) NOT NULL,
	fecha_solicitud date NOT NULL,
	descripcion_solicitud varchar(500) NOT NULL,
	estado_solicitud varchar(20) NOT NULL,
	tipo_queja varchar(30) NOT NULL,
	id_pasajero varchar(20) NOT NULL REFERENCES pasajeros (id_pasajero)
		ON UPDATE CASCADE ON DELETE NO ACTION,
	id_estacion integer NOT NULL REFERENCES estaciones (id_estacion)
		ON UPDATE CASCADE ON DELETE NO ACTION,
	id_empleado_atiende varchar(20) NOT NULL REFERENCES empleados (id_empleado)
		ON UPDATE CASCADE ON DELETE NO ACTION
);

CREATE TABLE medidas_solucion
(
	n_tiquete integer,
	medida_solucion varchar(500) NOT NULL,
	CONSTRAINT medida_sol_pk PRIMARY KEY (n_tiquete, medida_solucion),
	CONSTRAINT medida_sol_fk FOREIGN KEY (n_tiquete) REFERENCES solicitudes (n_tiquete)
		ON UPDATE CASCADE ON DELETE NO ACTION
);

CREATE TABLE ingresos
(
	id_pasajero varchar(20) NOT NULL REFERENCES pasajeros (id_pasajero)
		ON UPDATE CASCADE ON DELETE NO ACTION,
	id_ruta integer  NOT NULL REFERENCES rutas (id_ruta)
		ON UPDATE CASCADE ON DELETE NO ACTION,
	fecha date NOT NULL,
	hora time NOT NULL,
	CONSTRAINT ingresos_pk PRIMARY KEY (id_pasajero,id_ruta,fecha,hora)
);

CREATE TABLE rutas_pasan
(
	id_ruta integer NOT NULL REFERENCES rutas (id_ruta)
		ON UPDATE CASCADE ON DELETE NO ACTION,
	id_estacion integer NOT NULL REFERENCES estaciones (id_estacion)
		ON UPDATE CASCADE ON DELETE NO ACTION,
	CONSTRAINT rut_pasan_pk PRIMARY KEY (id_estacion,id_ruta)
);


CREATE TABLE turnos(
	id_empleado varchar(20) NOT NULL REFERENCES empleados (id_empleado)
		ON UPDATE CASCADE ON DELETE NO ACTION,
	placa_bus varchar(10) NOT NULL REFERENCES buses (placa)
		ON UPDATE CASCADE ON DELETE NO ACTION,
	fecha date NOT NULL,
	horario varchar(20) NOT NULL, 
	CONSTRAINT turnos_pk PRIMARY KEY (id_empleado, placa_bus, fecha, horario)

);


CREATE TABLE recargas(
	id_estacion integer NOT NULL REFERENCES estaciones(id_estacion),
	pin_tarjetas integer NOT NULL REFERENCES tarjetas(pin),
	valor_recarga integer,
	fecha date NOT NULL, 
	hora time NOT NULL,
	CONSTRAINT recargas_pk PRIMARY KEY (id_estacion, pin_tarjetas, valor_recarga, fecha, hora)
);

	
/************** Empleados **************/
--- GERENTE
INSERT INTO empleados VALUES ( '114444446', 'juan','morales', 'gerente', 20000000,'activo', '114444446', 'juan','gerente');

ALTER TABLE empleados ADD CONSTRAINT emp_fk FOREIGN KEY (id_empleado_controla) REFERENCES empleados (id_empleado)
		ON UPDATE CASCADE ON DELETE NO ACTION;

--- DIR OPERATIVO
INSERT INTO empleados VALUES ( '1144088585', 'sara','munoz', 'director operativo', 3500000,'activo', '114444446', 'sara','operativo');

--- DIR ESTACION
INSERT INTO empleados VALUES ( '1144025254', 'jhon','mena', 'director estacion', 2500000,'activo', '114444446', 'jhon','estacion');
INSERT INTO empleados VALUES ( '385666666', 'esteban','aguirre', 'director estacion', 2500000,'activo', '114444446', 'esteban','estacion');
INSERT INTO empleados VALUES ( '1144666668', 'pablo','moreno', 'director estacion', 2500000,'activo', '114444446', 'pablom','estacion');
INSERT INTO empleados VALUES ( '114456669', 'fabio','castaneda', 'director estacion', 2500000,'activo', '114444446', 'fabio','estacion');
INSERT INTO empleados VALUES ( '114453369', 'carlos','lopez', 'director estacion', 2500000,'activo', '114444446', 'carlos','estacion');
INSERT INTO empleados VALUES ( '114452269', 'antonio','villota', 'director estacion', 2500000,'activo', '114444446', 'antonio','estacion');

--- AUXILIARES ---
INSERT INTO empleados VALUES ( '1144083002', 'annie','Munoz', 'auxiliar', 1200000,'activo', '114444446', 'annie','auxiliar');
INSERT INTO empleados VALUES ( '1125987456', 'paola','revelo', 'auxiliar', 1200000,'activo', '114444446', 'paola','auxiliar');
INSERT INTO empleados VALUES ( '1157489456', 'carol','rodriguez', 'auxiliar', 1200000,'activo', '114444446', 'carol','auxiliar');
INSERT INTO empleados VALUES ( '1587993422', 'juan','perez', 'auxiliar', 1200000,'activo', '114444446', 'juanp','auxiliar');
INSERT INTO empleados VALUES ( '1189754853', 'jhonier','calero', 'auxiliar', 1200000,'activo', '114444446', 'jhonier','auxiliar');
INSERT INTO empleados VALUES ( '1069987542', 'pablo','gonzales', 'auxiliar', 1200000,'activo', '114444446', 'pablo','auxiliar');
INSERT INTO empleados VALUES ( '3159844542', 'joan','tovar', 'auxiliar', 1200000,'activo', '114444446', 'joan','auxiliar');
INSERT INTO empleados VALUES ( '1269987572', 'carlos','gutierrez', 'auxiliar', 1200000,'activo', '114444446', 'carlosg','auxiliar');
INSERT INTO empleados VALUES ( '3159822252', 'steven','puchumi', 'auxiliar', 1200000,'activo', '114444446', 'steven','auxiliar');

--- CONDUCTORES ---
INSERT INTO empleados VALUES ( '115984352', 'alejandro','ruiz', 'conductor', 1200000,'activo', '114444446', 'alejandro','conductor');
INSERT INTO empleados VALUES ( '315984455', 'alfredo','molina', 'conductor', 1200000,'activo', '114444446', 'alfredo','conductor');
INSERT INTO empleados VALUES ( '315984456', 'miguel','arteaga', 'conductor', 1200000,'activo', '114444446', 'miguel','conductor');
INSERT INTO empleados VALUES ( '315984457', 'ingrid','paz', 'conductor', 1200000,'activo', '114444446', 'ingrid','conductor');
INSERT INTO empleados VALUES ( '315984458', 'marcela','erazo', 'conductor', 1200000,'activo', '114444446', 'marcela','conductor');
INSERT INTO empleados VALUES ( '315984459', 'daniela','delgado', 'conductor', 1200000,'activo', '114444446', 'daniela','conductor');

/************** Estaciones **************/

INSERT INTO estaciones (nombre_estacion, ubicacion_estacion, id_empleado) VALUES ('tequendama','calle 48 No 45 - 16','1144025254');
INSERT INTO estaciones (nombre_estacion, ubicacion_estacion, id_empleado) VALUES ('refugio','calle 18 No 51 - 21','385666666');
INSERT INTO estaciones (nombre_estacion, ubicacion_estacion, id_empleado) VALUES ('caldas','calle 23 No 60 - 46','1144666668');
INSERT INTO estaciones (nombre_estacion, ubicacion_estacion, id_empleado) VALUES ('capri','calle 15 No 47 - 28','114456669');
INSERT INTO estaciones (nombre_estacion, ubicacion_estacion, id_empleado) VALUES ('centro','calle 36 No 55 - 27','114453369');
INSERT INTO estaciones (nombre_estacion, ubicacion_estacion, id_empleado) VALUES ('buitrera','calle 13 No 42 - 51','114452269');

/************** Tarjetas **************/

INSERT INTO tarjetas (saldo, tipo, estado, id_estacion)VALUES(6000, 'personal', 'activa', 1);
INSERT INTO tarjetas (saldo, tipo, estado, id_estacion)VALUES(12000, 'personal', 'activa', 2);
INSERT INTO tarjetas (saldo, tipo, estado, id_estacion)VALUES(7500, 'personal', 'activa', 3);
INSERT INTO tarjetas (saldo, tipo, estado, id_estacion)VALUES(13300, 'personal', 'activa', 3);
INSERT INTO tarjetas (saldo, tipo, estado, id_estacion)VALUES(6800, 'personal', 'activa', 3);
INSERT INTO tarjetas (saldo, tipo, estado, id_estacion)VALUES(9500, 'generica', 'activa', 4);


/************** Rutas **************/

INSERT INTO rutas (nombre_ruta, descripcion_recorrido, estado_ruta) VALUES ('P27C', '/buitrera/capri/refugio', 'activa');
INSERT INTO rutas (nombre_ruta, descripcion_recorrido, estado_ruta) VALUES ('E21', '/buitrera/capri/caldas/tequendama', 'activa');
INSERT INTO rutas (nombre_ruta, descripcion_recorrido, estado_ruta) VALUES ('E31', '/buitrera/cento/tequendama', 'activa');
INSERT INTO rutas (nombre_ruta, descripcion_recorrido, estado_ruta) VALUES ('P10D', '/buitrera/refugio', 'activa');
INSERT INTO rutas (nombre_ruta, descripcion_recorrido, estado_ruta) VALUES ('T47B', '/centro/tequendama', 'activa');
INSERT INTO rutas (nombre_ruta, descripcion_recorrido, estado_ruta) VALUES ('P10A', '/buitrera/capri/caldas', 'activa');
INSERT INTO rutas (nombre_ruta, descripcion_recorrido, estado_ruta) VALUES ('T31', '/buitrera/caldas/capri/centro/refugio', 'activa');

/************** Buses **************/

INSERT INTO buses VALUES ('ABC-001', 'alimentador', 'activo');
INSERT INTO buses VALUES ('ABC-002', 'alimentador', 'activo');
INSERT INTO buses VALUES ('ABC-003', 'articulado', 'activo');
INSERT INTO buses VALUES ('ABC-004', 'articulado', 'activo');
INSERT INTO buses VALUES ('ABC-005', 'padron', 'activo');
INSERT INTO buses VALUES ('ABC-006', 'padron', 'activo');
INSERT INTO buses VALUES ('ABC-007', 'padron', 'activo');

/************** Ventas **************/

INSERT INTO ventas (pin_tarjeta, id_empleado, id_estacion, fecha, costo)VALUES ('1','1144083002','1','30-11-2016', 3000);
INSERT INTO ventas (pin_tarjeta, id_empleado, id_estacion, fecha, costo)VALUES ('2','1125987456','2','29-11-2016', 3000);
INSERT INTO ventas (pin_tarjeta, id_empleado, id_estacion, fecha, costo)VALUES ('3','3159844542','3','28-11-2016', 3000);
INSERT INTO ventas (pin_tarjeta, id_empleado, id_estacion, fecha, costo)VALUES ('4','1587993422','4','1-12-2016', 3000);
INSERT INTO ventas (pin_tarjeta, id_empleado, id_estacion, fecha, costo)VALUES ('5','1189754853','5','2-12-2016', 3000);
INSERT INTO ventas (pin_tarjeta, id_empleado, id_estacion, fecha, costo)VALUES ('6','1069987542','6','3-12-2016', 3000);

/************** Pasajeros **************/

INSERT INTO pasajeros VALUES ('1144076634','1', 'pedro','suarez','3113954144');
INSERT INTO pasajeros VALUES ('1144076635','2', 'fernando','acosta','3003954444');
INSERT INTO pasajeros VALUES ('1144076636','3', 'maria','cruz','3113234145');
INSERT INTO pasajeros VALUES ('1144076637','4', 'gabriela','martinez','3113912345');
INSERT INTO pasajeros VALUES ('1144076638','5', 'susana','bedoya','3207215448');

/************** Solicitudes **************/

INSERT INTO solicitudes (motivo_solicitud, fecha_solicitud, descripcion_solicitud, estado_solicitud, tipo_queja, id_pasajero, id_estacion, id_empleado_atiende)
	VALUES ('Frecuencia de rutas', '30-11-2016', ' la P10A se demora mucho en llegar, hay que esperarla más de una hora. La hora que dice en el tablero no se cumple.', 'iniciado', 'concurrencia', '1144076634','1','1144083002');
INSERT INTO solicitudes (motivo_solicitud, fecha_solicitud, descripcion_solicitud, estado_solicitud, tipo_queja, id_pasajero, id_estacion, id_empleado_atiende)
	VALUES ('Atención al cliente', '30-11-2016', ' las personas encargadas de recargar y vender las tarjetas no siempre estan presentes en su puesto de trabajo.', 'iniciado', 'concurrencia', '1144076636','1','1144083002');
INSERT INTO solicitudes (motivo_solicitud, fecha_solicitud, descripcion_solicitud, estado_solicitud, tipo_queja, id_pasajero, id_estacion, id_empleado_atiende)
	VALUES ('Atención al cliente', '1-12-2016', ' el trato de los conductores no es adecuado con los pasajeros', 'en proceso', 'servicio', '1144076635','2','1125987456');
INSERT INTO solicitudes (motivo_solicitud, fecha_solicitud, descripcion_solicitud, estado_solicitud, tipo_queja, id_pasajero, id_estacion, id_empleado_atiende)
	VALUES ('Estado estaciones', '2-12-2016', ' las estaciones no se estan limpias, y las puertas no sirven', 'termiando', 'concurrencia', '1144076636','3','1157489456');

/************** Medidas_solucion **************/

INSERT INTO medidas_solucion VALUES ('1', 'llamar al pasajero');
INSERT INTO medidas_solucion VALUES ('1', 'aumentar concurrencia ruta P10A');
INSERT INTO medidas_solucion VALUES ('2', 'llamar al pasajero');
INSERT INTO medidas_solucion VALUES ('2', 'llamar al conductor');
INSERT INTO medidas_solucion VALUES ('3', 'llamar al pasajero');
INSERT INTO medidas_solucion VALUES ('3', 'aumentar frecuencia de limpieza de estaciones');

/************** Ingresos **************/

INSERT INTO ingresos VALUES ('1144076634','1','30-11-2016','4:23:07');
INSERT INTO ingresos VALUES ('1144076634','2','30-11-2016','16:54:15');
INSERT INTO ingresos VALUES ('1144076634','3','1-12-2016','6:27:33');

INSERT INTO ingresos VALUES ('1144076635','5','30-11-2016','4:23:07');
INSERT INTO ingresos VALUES ('1144076635','4','1-12-2016','18:45:52');
INSERT INTO ingresos VALUES ('1144076635','6','2-12-2016','7:23:23');

INSERT INTO ingresos VALUES ('1144076636','7','30-11-2016','17:34:56');
INSERT INTO ingresos VALUES ('1144076636','1','2-12-2016','5:23:34');
INSERT INTO ingresos VALUES ('1144076636','2','3-12-2016','6:40:21');

INSERT INTO ingresos VALUES ('1144076637','3','30-11-2016','6:45:43');
INSERT INTO ingresos VALUES ('1144076637','2','3-12-2016','15:40:56');
INSERT INTO ingresos VALUES ('1144076637','3','4-12-2016','17:26:43');

/************** Rutas_pasan **************/

INSERT INTO rutas_pasan VALUES ('1','6');
INSERT INTO rutas_pasan VALUES ('1','4');
INSERT INTO rutas_pasan VALUES ('1','2');

INSERT INTO rutas_pasan VALUES ('2','6');
INSERT INTO rutas_pasan VALUES ('2','4');
INSERT INTO rutas_pasan VALUES ('2','3');
INSERT INTO rutas_pasan VALUES ('2','1');

INSERT INTO rutas_pasan VALUES ('3','6');
INSERT INTO rutas_pasan VALUES ('3','5');
INSERT INTO rutas_pasan VALUES ('3','1');

INSERT INTO rutas_pasan VALUES ('4','6');
INSERT INTO rutas_pasan VALUES ('4','2');

INSERT INTO rutas_pasan VALUES ('5','5');
INSERT INTO rutas_pasan VALUES ('5','1');

INSERT INTO rutas_pasan VALUES ('6','6');
INSERT INTO rutas_pasan VALUES ('6','4');
INSERT INTO rutas_pasan VALUES ('6','3');

INSERT INTO rutas_pasan VALUES ('7','6');
INSERT INTO rutas_pasan VALUES ('7','3');
INSERT INTO rutas_pasan VALUES ('7','4');
INSERT INTO rutas_pasan VALUES ('7','5');
INSERT INTO rutas_pasan VALUES ('7','2');

/************** Turnos **************/

INSERT INTO turnos VALUES ('115984352', 'ABC-001', '30-11-2016', '5:00 - 11:00');
INSERT INTO turnos VALUES ('115984352', 'ABC-004', '1-12-2016', '11:00 - 17:00');

INSERT INTO turnos VALUES ('315984455', 'ABC-006', '30-11-2016', '11:00 - 17:00');
INSERT INTO turnos VALUES ('315984455', 'ABC-001', '1-12-2016', '5:00 - 11:00');

INSERT INTO turnos VALUES ('315984456', 'ABC-005', '30-11-2016', '17:00 - 23:00');
INSERT INTO turnos VALUES ('315984456', 'ABC-007', '1-12-2016', '11:00 - 17:00');

INSERT INTO turnos VALUES ('315984457', 'ABC-004', '30-11-2016', '11:00 - 17:00');
INSERT INTO turnos VALUES ('315984457', 'ABC-007', '1-12-2016', '11:00 - 17:00');

/************** Recargas **************/

INSERT INTO recargas  VALUES (1,1, 5000, '30-11-2016' ,'17:23:04');
INSERT INTO recargas  VALUES (2,1, 1000, '30-12-2016' ,'4:00:10');
INSERT INTO recargas  VALUES (3,2, 10000, '29-11-2016' ,'9:10:03');
INSERT INTO recargas  VALUES (4,2, 2000, '14-12-2016' ,'20:15:07');
INSERT INTO recargas  VALUES (5,3, 3000, '28-11-2016' ,'14:55:40');
INSERT INTO recargas  VALUES (6,3, 4500, '19-12-2016' ,'13:45:14');
INSERT INTO recargas  VALUES (1,4, 7000, '1-12-2016' ,'21:32:22');
INSERT INTO recargas  VALUES (2,4, 6300, '31-12-2016' ,'22:10:03');
INSERT INTO recargas  VALUES (3,5, 1800, '2-12-2016' ,'7:23:50');
INSERT INTO recargas  VALUES (4,5, 5000, '12-12-2016' ,'8:02:07');
INSERT INTO recargas  VALUES (5,6, 9000, '3-12-2016' ,'15:26:30');
INSERT INTO recargas  VALUES (6,6, 500, '11-12-2016' ,'16:40:09');









